//
//  CoreDataManager.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 24/05/23.
//

import Foundation
import CoreData
import FirebaseFirestore

class CoreDataManager {
    static let shared = CoreDataManager()
    
    @Published var restaurantsEntities: [RestaurantEntity] = []
    
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CoreDataModels")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    private func saveData(){
        do{
            try persistentContainer.viewContext.save()
        } catch _{
            print("Error saving the data to core data")
        }
    }
    
    // ------- FAVOURITE RESTAURANTS ------------
    func fetchAllRestaurants() -> [Restaurant]{
        do{
            let request = NSFetchRequest<RestaurantEntity>(entityName: "RestaurantEntity")
            restaurantsEntities = try persistentContainer.viewContext.fetch(request)
            restaurantsEntities.sort{ $0.position < $1.position }
            
            return entityToModel()
            
        } catch _{
            print("The error is when fetching")
        }
        return entityToModel()
    }
    
    func addRestaurant(username: String, restaurant: Restaurant, totalRestaurants: Int){
        
        var dishes:[DishEntity] = []
        for dish in restaurant.dishes{
            dishes.append(addDishes(dish: dish))
        }
        
        var reviews: [ReviewEntity] = []
        for review in restaurant.reviews{
            reviews.append(addReview(review: review))
        }
        
        let newRestaurant = RestaurantEntity(context: persistentContainer.viewContext)
        
        newRestaurant.id = restaurant.id
        newRestaurant.name = restaurant.name
        newRestaurant.cleannessScore = restaurant.cleannessScore
        newRestaurant.categories = restaurant.categories as NSObject
        newRestaurant.imageName = restaurant.imageName
        newRestaurant.latitude = restaurant.coordinates.latitude
        newRestaurant.longitude = restaurant.coordinates.longitude
        newRestaurant.stars = Int16(restaurant.stars)
        newRestaurant.recommendationScore = restaurant.recommendationScore
        newRestaurant.dishes = NSSet(array: dishes)
        newRestaurant.reviews = NSSet(array: reviews)
        newRestaurant.position = Int16(totalRestaurants)
        
        saveData()
        
    }
    
    private func addDishes(dish: Dish) -> DishEntity{
        
        let newDish = DishEntity(context: persistentContainer.viewContext)
        newDish.id = dish.id
        newDish.name = dish.name
        newDish.imageName = dish.imageName
        newDish.preparationTime = Int16(dish.preparationTime)
        newDish.price = dish.price
        newDish.stars = Int16(dish.stars)
        
        saveData()
        
        return newDish
    }
    
    private func addReview(review: Review) -> ReviewEntity{
        
        let newReview = ReviewEntity(context: persistentContainer.viewContext)
        newReview.id = review.id
        newReview.stars = Int16(review.stars)
        newReview.user = review.user
        newReview.text = review.text
        
        saveData()
        
        return newReview
    }
    
    public func removeRestaurant(restaurant: Restaurant){
        let request = NSFetchRequest<RestaurantEntity>(entityName: "RestaurantEntity")
        let filter = NSPredicate(format: "name==%@", restaurant.name)
        request.predicate = filter
        
        do{
            let fetchedRestaurant = try persistentContainer.viewContext.fetch(request)
            
            if let restaurantToDelete = fetchedRestaurant.first {
                // Deleting the fetched restaurant from the context
                persistentContainer.viewContext.delete(restaurantToDelete)
                let index = Int(restaurantToDelete.position)
                if index + 1 < restaurantsEntities.count{
                    for i in (index+1)..<restaurantsEntities.count{
                        restaurantsEntities[i].position -= 1
                    }
                }
                saveData()
            }else{
                print("No restaurant with name: \(restaurant.name)")
            }
            
        } catch _{
            print("Error retreiving restaurant for removing from Core Data")
        }
    }
    
    public func reorderRestaurants(from: IndexSet, destination:Int){
        guard let itemToMove = from.first else {return}
        
        // Moving downwards
        if itemToMove < destination{
            var startIndex = itemToMove + 1
            let endIndex = destination - 1
            var startOrder = restaurantsEntities[itemToMove].position
            while startIndex <= endIndex{
                restaurantsEntities[startIndex].position = startOrder
                startOrder += 1
                startIndex += 1
            }
            restaurantsEntities[itemToMove].position = startOrder
        }
        // Moving upwards
        else if itemToMove > destination{
            var startIndex = destination
            let endIndex = itemToMove - 1
            var starOrder = restaurantsEntities[destination].position + 1
            let newOrder = restaurantsEntities[destination].position
            while startIndex <= endIndex{
                restaurantsEntities[startIndex].position = starOrder
                starOrder += 1
                startIndex += 1
            }
            restaurantsEntities[itemToMove].position = newOrder
        }
        
        saveData()
    }
    
    func isFavourite(restaurant: Restaurant) -> Bool{
        if restaurantsEntities.first(where: { $0.name == restaurant.name }) != nil {
            return true
        } else {
            return false
        }
    }
    
    private func entityToModel() -> [Restaurant]{
        
        var temp:[Restaurant] = []
        var dishes: [Dish] = []
        var reviews: [Review] = []
        var categories:[String]=[]
        
        for entity in restaurantsEntities{
            dishes = dishesPerRestaurant(restaurant: entity)
            reviews = reviewsPerRestaurant(restaurant: entity)
            if let categoriesObject = entity.categories{
                if let categoriesArray = categoriesObject as? [String] {
                    categories = categoriesArray
                } else if let categoriesSet = categoriesObject as? NSSet {
                    let categoriesArray = categoriesSet.compactMap { $0 as? String }
                    categories = categoriesArray
                }
            }
            temp.append(
                Restaurant(id: entity.id, name: entity.name ?? "NO NAME", stars: Int(entity.stars), cleannessScore: entity.cleannessScore, categories: categories, dishes: dishes, imageName: entity.imageName ?? "NO IMAGE", coordinates: GeoPoint(latitude: entity.latitude, longitude: entity.longitude), recommendationScore: entity.recommendationScore, reviews: reviews))
        }
        
         return temp
    }
    
    private func dishesPerRestaurant(restaurant: RestaurantEntity) -> [Dish]{
        var dishes: [Dish] = []
        
        if let dishSet = restaurant.dishes as? Set<DishEntity> {
            dishSet.forEach { dishEntity in
                dishes.append(Dish(id: dishEntity.id ?? "NO ID", name: dishEntity.name ?? "NO NAME", stars: Int(dishEntity.stars), preparationTime: Int(dishEntity.preparationTime), price: dishEntity.price, imageName: dishEntity.imageName ?? "NO IMAGE"))
            }
        } else{
            print("The restaurant doesn't has dishes")
        }
        
        return dishes
        
    }
    
    private func reviewsPerRestaurant(restaurant: RestaurantEntity) -> [Review]{
        var reviews: [Review] = []
        
        if let reviewSet = restaurant.reviews as? Set<ReviewEntity> {
            reviewSet.forEach { reviewEntity in
                reviews.append(Review(id: reviewEntity.id ?? "NO ID", user: reviewEntity.user ?? "NO USERNAME", stars: Int(reviewEntity.stars), text: reviewEntity.text ?? "NO TEXT"))
            }
        } else{
            print("The restaurant doesn't has dishes")
        }
        
        return reviews
        
    }
    
    
    // ------- BUDGET ------------
    func budget(startDate: Date, budgetMoney: Int64, savings: Double) -> Budget {
        let budget = Budget(context: persistentContainer.viewContext)
        budget.startDate = startDate
        budget.budget = budgetMoney
        budget.savings = savings
        return budget
    }
    
    func budgetDay(fullName: String, shortName: String, budget: Budget) -> BudgetDay {
        let budgetDay = BudgetDay(context: persistentContainer.viewContext)
        budgetDay.fullName = fullName
        budgetDay.shortName = shortName
        budget.addToDays(budgetDay)
        return budgetDay
    }
    
    func budgetDish(restaurant: String, name: String, price: Double, imageURL: String, budgetDay: BudgetDay) -> BudgetDish {
        let budgetDish = BudgetDish(context: persistentContainer.viewContext)
        budgetDish.restaurant = restaurant
        budgetDish.name = name
        budgetDish.price = price
        budgetDish.imageURL = imageURL
        budgetDay.addToDishes(budgetDish)
        return budgetDish
    }
    
    func budgetHistory() -> [Budget] {
        let request: NSFetchRequest<Budget> = Budget.fetchRequest()
        var fetchedBudgets: [Budget] = []
        
        do {
            fetchedBudgets = try persistentContainer.viewContext.fetch(request)
        } catch let error {
            print("Error fetching budget history \(error)")
        }
        return fetchedBudgets
    }
    
    func budgetsByStartDate(_ startDate: Date) -> [Budget] {
        let request: NSFetchRequest<Budget> = Budget.fetchRequest()
        request.predicate = NSPredicate(format: "startDate >= %@", startDate as NSDate)
        var fetchedBudgets: [Budget] = []
        
        do {
            fetchedBudgets = try persistentContainer.viewContext.fetch(request)
        } catch let error {
            print("Error fetching current budget \(error)")
        }
        return fetchedBudgets
    }
    
    // Core Data Saving support
    func save () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("❗️Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func deleteBudget(budget: Budget) {
        let context = persistentContainer.viewContext
        context.delete(budget)
        save()
    }
    

}
