//
//  DishRepository.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 21/03/23.
//

import FirebaseFirestore
import FirebaseFirestoreSwift
import SwiftUI
import Combine

final class DishRepository: ObservableObject {
    private let restaurantPath = "Restaurants/"
    private let dishPath = "Dishes"
    private let db = Firestore.firestore()
    @Published var dishes: [Dish] = []
    
    init(_ restaurantId: String) {
        get(by: restaurantId)
    }
    
    func get(by restaurantId: String) {
        db.collection(restaurantPath+restaurantId+"/"+dishPath).addSnapshotListener { (snapshot, error) in
            if let error = error {
                // TODO: handle error
                print(error)
                return
            }
            self.dishes = snapshot?.documents.compactMap {
                try? $0.data(as: Dish.self)
            } ?? []
        }
    }
    
}
