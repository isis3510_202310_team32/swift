//
//  RestaurantRepository.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 20/03/23.
//

import FirebaseFirestore
import FirebaseFirestoreSwift
import Combine

final class RestaurantFirebaseServiceAdapter: ObservableObject {
    private let path = "Restaurants"
    private let db = Firestore.firestore()
    @Published var restaurants: [Restaurant] = []
    
    init() {
        get()
    }
    
    func get() {
        db.collection(path).addSnapshotListener { (snapshot, error) in
            if let error = error {
                // TODO: handle error
                print(error)
                return
            }
            self.restaurants = snapshot?.documents.compactMap {
                try? $0.data(as: Restaurant.self)
            } ?? []
        }
    }
    
    func getRestaurant(id restaurantId: String) -> Restaurant? {
        let docRef = db.collection(path).document(restaurantId)
        var restaurantResult: Restaurant?
        docRef.getDocument(as: Restaurant.self) { result in
            switch result {
            case .success(let restaurant):
                restaurantResult = restaurant
            case .failure(let error):
                print("Error decoding document: \(error.localizedDescription)")
            }
        }
        return restaurantResult
    }
    
}
