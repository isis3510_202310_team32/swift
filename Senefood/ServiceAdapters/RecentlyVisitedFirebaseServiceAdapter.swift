//
//  RecentlyVisitedRepository.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 23/03/23.
//

import FirebaseFirestore
import FirebaseFirestoreSwift
import Combine

final class RecentlyVisitedFirebaseServiceAdapter: ObservableObject {
    private let path = "RecentlyVisitedPerUser"
    private let db = Firestore.firestore()
    @Published var recentlyVisitedRestaurantsPerUser: RecentlyVisitedPerUser?
    
    func get(by username: String) {
        let docRef = db.collection(path).document(username)
        docRef.getDocument(as: RecentlyVisitedPerUser.self) { result in
            switch result {
            case .success(let recentlyVisited):
                self.recentlyVisitedRestaurantsPerUser = recentlyVisited
                self.deleteMoreThanWeeksRestaurant()
                self.updateDocument(by: username, new: self.recentlyVisitedRestaurantsPerUser!)
            case .failure(let error):
                print("Error decoding document: \(error.localizedDescription)")
            }
        }
    }
    
    private func deleteMoreThanWeeksRestaurant() {
        let weeksAgo: Date = Calendar.current.date(byAdding: .day, value: -14, to: Date())!
        self.recentlyVisitedRestaurantsPerUser?.recentlyVisitedRestaurants = self.recentlyVisitedRestaurantsPerUser?.recentlyVisitedRestaurants.filter {
            $0.date > weeksAgo
        } ?? []
    }
    
    private func updateDocument(by username: String, new newDocument: RecentlyVisitedPerUser) {
        let docRef = db.collection(path).document(username)
        do {
            try docRef.setData(from: newDocument)
        }
        catch {
            print(error)
        }
    }
    
    func addRecentlyVisitedRestaurant(to username: String, and restaurantId: String) {
        if self.recentlyVisitedRestaurantsPerUser != nil {
            let newDocument = RecentlyVisitedRestaurant(date: Date(), restaurant: restaurantId)
            self.recentlyVisitedRestaurantsPerUser!.recentlyVisitedRestaurants.insert(newDocument, at: 0)
            self.updateDocument(by: username, new: self.recentlyVisitedRestaurantsPerUser!)
        }
    }
    
    func getRestaurantIds() -> [String] {
        var restaurantIds: [String] = []
        if self.recentlyVisitedRestaurantsPerUser != nil {
            restaurantIds = self.recentlyVisitedRestaurantsPerUser!.recentlyVisitedRestaurants.map { $0.restaurant }
        }
        return restaurantIds
    }
}
