//
//  RecommendationsServiceAdapter.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 1/04/23.
//

import Combine
import FirebaseFirestore
import FirebaseFirestoreSwift

final class RecommendationsService: ObservableObject {
    private let path = "RecommendationsPerUser"
    private let db = Firestore.firestore()
    @Published var recommendationsPerUser: RecommendationsPerUser?

    func getRecommendationsPerUser(username: String, completion: @escaping (Result<RecommendationsPerUser, Error>) -> Void) {
        let docRef = db.collection("RecommendationsPerUser").document(username)
        docRef.getDocument { document, error in
            if let error = error {
                completion(.failure(error))
            } else if let document = document, document.exists {
                do {
                    let recommendations = try document.data(as: RecommendationsPerUser.self)
                    completion(.success(recommendations))
                } catch {
                    completion(.failure(error))
                }
            }
        }
    }

    func getRestaurantsById(ids: [String], completion: @escaping (Result<[Restaurant], Error>) -> Void) {
        db.collection("Restaurants")
            .whereField(FieldPath.documentID(), in: ids).getDocuments { querySnapshot, error in
                if let error = error {
                    completion(.failure(error))
                } else {
                    let restaurants = querySnapshot?.documents.compactMap {
                        try? $0.data(as: Restaurant.self)
                    } ?? []
                    completion(.success(restaurants))
                }
            }
    }

    func likeRestaurant(username: String, restaurantId: String, completion: @escaping (Result<Void, Error>) -> Void) {
        let batch = db.batch()

        let recommendationsDocRef = db.collection("RecommendationsPerUser").document(username)
        let restaurantDocRef = db.collection("Restaurants").document(restaurantId)

        recommendationsDocRef.getDocument { document, error in
            if let error = error {
                completion(.failure(error))
            } else if let document = document, document.exists, var recommendationPerUser = try? document.data(as: RecommendationsPerUser.self) {
                let index = recommendationPerUser.recommendations.firstIndex { $0.restaurant == restaurantId }
                recommendationPerUser.recommendations[index!].likes = 1
                let encoder = Firestore.Encoder()
                do {
                    let recommendations = try encoder.encode(recommendationPerUser)
                    print("The recommendations per user are now: \(recommendations)")
                    batch.updateData(recommendations, forDocument: recommendationsDocRef)

                    restaurantDocRef.getDocument { document, error in
                        if let error = error {
                            completion(.failure(error))
                        } else if let document = document, document.exists, let restaurant = try? document.data(as: Restaurant.self) {
                            batch.updateData(["recommendationScore": restaurant.recommendationScore + 1], forDocument: restaurantDocRef)
                            batch.commit { error in
                                if let error = error {
                                    completion(.failure(error))
                                } else {
                                    completion(.success(()))
                                }
                            }
                        }
                    }
                } catch {
                    print("Error enconding to map: \(error)")
                }
            }
        }
    }

    func dislikeRestaurant(username: String, restaurantId: String, completion: @escaping (Result<Void, Error>) -> Void) {
        let batch = db.batch()

        let recommendationsDocRef = db.collection("RecommendationsPerUser").document(username)
        let restaurantDocRef = db.collection("Restaurants").document(restaurantId)

        recommendationsDocRef.getDocument { document, error in
            if let error = error {
                completion(.failure(error))
            } else if let document = document, document.exists, var recommendationPerUser = try? document.data(as: RecommendationsPerUser.self) {
                let index = recommendationPerUser.recommendations.firstIndex { $0.restaurant == restaurantId }
                recommendationPerUser.recommendations[index!].likes = -1
                let encoder = Firestore.Encoder()
                do {
                    let recommendations = try encoder.encode(recommendationPerUser)
                    print("The recommendations per user are now: \(recommendations)")
                    batch.updateData(recommendations, forDocument: recommendationsDocRef)

                    restaurantDocRef.getDocument { document, error in
                        if let error = error {
                            completion(.failure(error))
                        } else if let document = document, document.exists, let restaurant = try? document.data(as: Restaurant.self) {
                            batch.updateData(["recommendationScore": restaurant.recommendationScore - 1], forDocument: restaurantDocRef)
                            batch.commit { error in
                                if let error = error {
                                    completion(.failure(error))
                                } else {
                                    completion(.success(()))
                                }
                            }
                        }
                    }
                } catch {
                    print("Error enconding to map: \(error)")
                }
            }
        }
    }

    func resetLikesRestaurantFromDislike(username: String, restaurantId: String, completion: @escaping (Result<Void, Error>) -> Void) {
        let batch = db.batch()

        let recommendationsDocRef = db.collection("RecommendationsPerUser").document(username)
        let restaurantDocRef = db.collection("Restaurants").document(restaurantId)

        recommendationsDocRef.getDocument { document, error in
            if let error = error {
                completion(.failure(error))
            } else if let document = document, document.exists, var recommendationPerUser = try? document.data(as: RecommendationsPerUser.self) {
                let index = recommendationPerUser.recommendations.firstIndex { $0.restaurant == restaurantId }
                recommendationPerUser.recommendations[index!].likes = 0
                let encoder = Firestore.Encoder()
                do {
                    let recommendations = try encoder.encode(recommendationPerUser)
                    print("The recommendations per user are now: \(recommendations)")
                    batch.updateData(recommendations, forDocument: recommendationsDocRef)

                    restaurantDocRef.getDocument { document, error in
                        if let error = error {
                            completion(.failure(error))
                        } else if let document = document, document.exists, let restaurant = try? document.data(as: Restaurant.self) {
                            batch.updateData(["recommendationScore": restaurant.recommendationScore + 1], forDocument: restaurantDocRef)
                            batch.commit { error in
                                if let error = error {
                                    completion(.failure(error))
                                } else {
                                    completion(.success(()))
                                }
                            }
                        }
                    }
                } catch {
                    print("Error enconding to map: \(error)")
                }
            }
        }
    }

    func resetLikesRestaurantFromLike(username: String, restaurantId: String, completion: @escaping (Result<Void, Error>) -> Void) {
        let batch = db.batch()

        let recommendationsDocRef = db.collection("RecommendationsPerUser").document(username)
        let restaurantDocRef = db.collection("Restaurants").document(restaurantId)

        recommendationsDocRef.getDocument { document, error in
            if let error = error {
                completion(.failure(error))
            } else if let document = document, document.exists, var recommendationPerUser = try? document.data(as: RecommendationsPerUser.self) {
                let index = recommendationPerUser.recommendations.firstIndex { $0.restaurant == restaurantId }
                recommendationPerUser.recommendations[index!].likes = 0
                let encoder = Firestore.Encoder()
                do {
                    let recommendations = try encoder.encode(recommendationPerUser)
                    print("The recommendations per user are now: \(recommendations)")
                    batch.updateData(recommendations, forDocument: recommendationsDocRef)

                    restaurantDocRef.getDocument { document, error in
                        if let error = error {
                            completion(.failure(error))
                        } else if let document = document, document.exists, let restaurant = try? document.data(as: Restaurant.self) {
                            batch.updateData(["recommendationScore": restaurant.recommendationScore - 1], forDocument: restaurantDocRef)
                            batch.commit { error in
                                if let error = error {
                                    completion(.failure(error))
                                } else {
                                    completion(.success(()))
                                }
                            }
                        }
                    }
                } catch {
                    print("Error enconding to map: \(error)")
                }
            }
        }
    }
}
