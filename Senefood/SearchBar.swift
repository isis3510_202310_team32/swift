//
//  SearchBar.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 7/03/23.
//

import SwiftUI

struct SearchBar: View {
    @Binding var searchText: String
    var body: some View {
        HStack {
            Image(systemName: "magnifyingglass")
                .foregroundColor(searchText.isEmpty ?
                    Color.secondary : Color.accentColor)

            TextField("Search", text: $searchText)
                .foregroundColor(Color.accentColor)
                .overlay(
                    Image(systemName: "xmark.circle.fill")
                        .padding()
                        .offset(x: 10)
                        .foregroundColor(Color.accentColor)
                        .opacity(searchText.isEmpty ? 0.0 : 1.0)
                        .onTapGesture {
                            searchText = ""
                        }, alignment: .trailing
                )
        }
        .font(.headline)
        .padding(7)
        .background(
            RoundedRectangle(cornerRadius: 25)
                .fill(Color("SearchbarColor"))
        )
    }
}

struct SearchBar_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            SearchBar(searchText: .constant(""))
                .previewLayout(.sizeThatFits)
                .preferredColorScheme(.light)
            SearchBar(searchText: .constant(""))
                .previewLayout(.sizeThatFits)
                .preferredColorScheme(.dark)
        }
    }
}
