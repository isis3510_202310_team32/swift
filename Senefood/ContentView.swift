//
//  ContentView.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 3/03/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            RestaurantList()
                .tabItem {
                    Label("Restaurants", systemImage: "fork.knife")
                }
            BudgetView()
                .tabItem {
                    Label("Budget", systemImage: "dollarsign.circle")
                }
            RestaurantList()
                .tabItem {
                    Label("Recommend", systemImage: "sparkles")
                }
            RestaurantList()
                .tabItem {
                    Label("Profile", systemImage: "person.crop.circle")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(ModelData())
    }
}
