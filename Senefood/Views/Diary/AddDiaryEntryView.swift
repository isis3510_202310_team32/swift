//
//  AddDiaryEntryView.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 21/05/23.
//

import SwiftUI

struct AddDiaryEntryView: View {
    var diaryVM: DiaryViewModel
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var loginVM: LoginViewModel
    @Environment(\.managedObjectContext) var moc
    @AppStorage("TitlePreference") private var titlePreference: String = ""
    @AppStorage("ContentPreference") private var contentPreference: String = ""

    @State private var title: String = ""
    @State private var content: String = ""
    @State private var selectedOption: String = ""
    @State private var loadedFromUserDefaults: Bool = false

    private let titleCharacterLimit = 50
    private let contentCharacterLimit = 250

    var body: some View {
        Form {
            if loadedFromUserDefaults {
                Section(header: Text(LocalizationCacheManager.shared.getLocalized(forKey: "diary_entry_not_saved"))) {}
                Button(action: {
                    title = ""
                    content = ""
                    titlePreference = ""
                    contentPreference = ""
                    loadedFromUserDefaults = false
                }) {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Delete content and write new entry"))
                }
            }
            Section(header: Text(LocalizationCacheManager.shared.getLocalized(forKey: "Restaurant visited"))) {
                if !diaryVM.isLoading {
                    if diaryVM.restaurants.count != 0 {
                        Picker(LocalizationCacheManager.shared.getLocalized(forKey: "Select option"), selection: $selectedOption) {
                            Text(LocalizationCacheManager.shared.getLocalized(forKey: "Please select a restaurant")).tag("")
                            ForEach(Array(diaryVM.restaurants.keys), id: \.self) { option in
                                Text(option)
                            }
                        }
                    } else {
                        Text(LocalizationCacheManager.shared.getLocalized(forKey: "Network error. No restaurants could be retrieved"))
                    }

                } else {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Loading restaurants..."))
                }
            }
            Section(header: Text(LocalizationCacheManager.shared.getLocalized(forKey: "Title") + " \(title.count)/50")) {
                TextField(LocalizationCacheManager.shared.getLocalized(forKey: "Enter title"), text: $title)
                    .onChange(of: title) { newValue in
                        if newValue.count > titleCharacterLimit {
                            title = String(newValue.prefix(titleCharacterLimit))
                        }
                        titlePreference = title // Store the title preference
                    }
            }

            Section(header: Text(LocalizationCacheManager.shared.getLocalized(forKey: "Content") + " \(content.count)/250")) {
                TextField(LocalizationCacheManager.shared.getLocalized(forKey: "Enter content"), text: $content, axis: .vertical)
                    .onChange(of: content) { newValue in
                        if newValue.count > contentCharacterLimit {
                            content = String(newValue.prefix(contentCharacterLimit))
                        }
                        contentPreference = content // Store the content preference
                    }
            }

            Button(action: saveToCoreData) {
                Text(LocalizationCacheManager.shared.getLocalized(forKey: "Save"))
            }.disabled(title.isEmpty || content.isEmpty || selectedOption.isEmpty)
        }
        .onAppear {
            diaryVM.fetchRestaurants()
            loadedFromUserDefaults = false
            let titlePref = titlePreference
            let contentPref = contentPreference

            if !titlePref.isEmpty || !contentPref.isEmpty {
                print("The preferencesValuesAre triggered to true")
                loadedFromUserDefaults = true
            }
            print("The preferencesValuesAre \(titlePref) and \(contentPref)")

            title = titlePref
            content = contentPref
        }
    }

    private func saveToCoreData() {
        let diaryEntry = DiaryEntry(context: moc)

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yy"
        let currentDate = dateFormatter.string(from: Date())

        diaryEntry.title = title
        diaryEntry.content = content
        diaryEntry.restaurantName = selectedOption
        diaryEntry.id = UUID()
        diaryEntry.createdBy = loginVM.user?.email
        diaryEntry.imageURL = diaryVM.restaurants[selectedOption]
        diaryEntry.date = currentDate
        defer {
            titlePreference = ""
            contentPreference = ""
            presentationMode.wrappedValue.dismiss()
        }
        do {
            try moc.save()
            print("Diary entry saved successfully!")
            print()
        } catch {
            print("Failed to save diary entry: \(error.localizedDescription)")
        }
    }
}

struct AddDiaryEntryView_Previews: PreviewProvider {
    static var previews: some View {
        AddDiaryEntryView(diaryVM: DiaryViewModel())
    }
}
