//
//  DiaryEntryDetail.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 21/05/23.
//

import Kingfisher
import SwiftUI
struct DiaryEntryDetail: View {
    let entry: DiaryEntry?
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                Text(entry?.title ?? "Title").font(.largeTitle)
                Text(LocalizationCacheManager.shared.getLocalized(forKey: "Restaurant visited:") + " \(entry?.restaurantName ?? "Restaurant name that can be longer")")
                HStack {
                    Spacer()
                    KFImage(URL(string: entry?.imageURL ?? "https://picsum.photos/id/63/400/400"))
                        .placeholder { Image("logo").resizable().frame(width: 200, height: 200) }
                        .loadDiskFileSynchronously()
                        .cacheMemoryOnly()
                        .fade(duration: 0.25)
                        .onSuccess { _ in
                            print("Succesfully retreived image")
                        }
                        .onFailure { _ in
                            print("Error in retreived image")
                        }.resizable()
                        .frame(width: 200, height: 200)
                    Spacer()
                }
                Text(entry?.content ?? "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sed dolor tempor, convallis arcu vitae, facilisis ex. Curabitur mattis ante ac vulputate lacinia. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae. Etiam venenatis porta lectus. Nulla vel dolor at eros molestie dapibus vitae id lectus.")
                    .padding(.bottom)
                Text(entry?.date ?? "21/05/23")
            }.padding()
        }
    }
}

struct DiaryEntryDetail_Previews: PreviewProvider {
    static var previews: some View {
        DiaryEntryDetail(entry: nil)
    }
}
