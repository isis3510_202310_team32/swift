//
//  DiaryMainView.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 21/05/23.
//

import CoreData
import SwiftUI

struct DiaryMainView: View {
    var diaryVM: DiaryViewModel
    @FetchRequest var diaryEntries: FetchedResults<DiaryEntry>
    init(diaryVM: DiaryViewModel, currentUserEmail: String) {
        self.diaryVM = diaryVM
        let request: NSFetchRequest<DiaryEntry> = DiaryEntry.fetchRequest()
        request.sortDescriptors = []
        request.predicate = NSPredicate(format: "createdBy == %@", currentUserEmail)
        _diaryEntries = FetchRequest(fetchRequest: request)
    }

    var body: some View {
        NavigationStack {
            VStack {
                if !diaryEntries.isEmpty {
                    List {
                        ForEach(diaryEntries) { entry in
                            NavigationLink(destination: DiaryEntryDetail(entry: entry)) {
                                DiaryEntryRow(entry: entry)
                            }
                        }
                    }.listStyle(.plain)
                } else {
                    List {
                        Text(LocalizationCacheManager.shared.getLocalized(forKey: "no_diary_entries"))
                    }.listStyle(.plain)
                }
            }.navigationTitle("Diary")
                .navigationBarItems(trailing: NavigationLink(destination: AddDiaryEntryView(diaryVM: diaryVM)) {
                    Image(systemName: "plus")
                        .navigationBarTitleDisplayMode(.inline)
                })
        }
    }
}

struct DiaryMainView_Previews: PreviewProvider {
    static var previews: some View {
        DiaryMainView(diaryVM: DiaryViewModel(), currentUserEmail: "")
    }
}
