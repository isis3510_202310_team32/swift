//
//  DiaryEntryRow.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 21/05/23.
//

import SwiftUI

struct DiaryEntryRow: View {
    let entry: DiaryEntry?
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(entry?.title ?? "Diary entry").fontWeight(.semibold)
                Text(entry?.date ?? "21/05/23").font(.caption)
            }
        }
    }
}

struct DiaryEntryRow_Previews: PreviewProvider {
    static var previews: some View {
        DiaryEntryRow(entry: nil)
    }
}
