//
//  ReviewRow.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 1/04/23.
//

import SwiftUI

struct ReviewRow: View {
    var review: Review
    var body: some View {
        VStack(alignment: .leading, spacing: 3) {
            Text(review.user).fontWeight(.semibold)
            ReviewStarsView(stars: .constant(review.stars))
            Text(review.text)
        }.padding()
    }
}

struct ReviewRow_Previews: PreviewProvider {
    static var previews: some View {
        ReviewRow(review: Review(id: "1", user: "Pepito Pérez", stars: 5, text: "The experience was awesome. The dish I ordered was great and I loved how it tasted. I will be back soon"))
    }
}
