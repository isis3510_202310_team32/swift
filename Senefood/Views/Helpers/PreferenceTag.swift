//
//  PreferenceTag.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 5/03/23.
//

import SwiftUI

struct PreferenceTag: View {
    var text: String

    var body: some View {
        Text(text.capitalized)
            .padding(10)
            .font(.body)
            .background(Color.accentColor)
            .foregroundColor(.white)
            .cornerRadius(10)
    }
}

struct PreferenceTag_Previews: PreviewProvider {
    static var previews: some View {
        PreferenceTag(text: "Text")
    }
}
