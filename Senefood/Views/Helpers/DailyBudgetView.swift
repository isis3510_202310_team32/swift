//
//  DailyBudgetView.swift
//  Senefood
//
//  Created by Daniel Rincon on 6/03/23.
//

import SwiftUI

struct DailyBudgetView: View {
    @State private var containerWidth: CGFloat = 0
    
    private let maxBudget = 150000
    
    let days: [String] =
        ["Mon", "Tue", "Wed", "Thu", "Fri"]
    
    let spentPerDay: [Int] =
        [35000, 45000, 15000, 25000, 40000]
    
    var body: some View {
        VStack(alignment: .leading) {
            ForEach(0 ..< 5) { day in
                HStack {
                    Text(days[day])
                        .frame(width: 50.0)
                        
                    ZStack(alignment: .leading) {
                        GeometryReader { geo in
                            RoundedRectangle(cornerRadius: 60)
                                .foregroundColor(.clear)
                                .onAppear {
                                    containerWidth = geo.size.width
                                }
                        }
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(.clear, lineWidth: 1)
                        ZStack(alignment: .trailing) {
                            RoundedRectangle(cornerRadius: 60)
                                .fill(Color("AccentColor"))
                            // Text("$"+String(spentPerDay[day]/1000)+".000")
                            Text(numberToMoney(number: spentPerDay[day]))
                                .foregroundColor(.white)
                                .padding(EdgeInsets(top: 6, leading: 12, bottom: 6, trailing: 12))
                                .background(
                                    RoundedRectangle(cornerRadius: 60)
                                        .fill(Color("AccentColor"))
                                )
                        }
                        .padding(2)
                        .frame(minWidth: min(CGFloat(spentPerDay[day]) * containerWidth / CGFloat(maxBudget), containerWidth))
                        .fixedSize()
                    }
                    .fixedSize(horizontal: false, vertical: true)
                    .padding(1)
                }
                Divider()
            }
        }
    }
    
    func numberToMoney(number: Int) -> String {
        var numberString = String(number)
        let hundreth = numberString.suffix(3)
        
        let index = numberString.index(numberString.endIndex, offsetBy: -3)
        let thousand = numberString[..<index] // Hello
        
        numberString = "$" + String(thousand) + "." + String(hundreth)
        return numberString
    }
}

struct DailyBudgetView_Previews: PreviewProvider {
    static var previews: some View {
        DailyBudgetView()
    }
}
