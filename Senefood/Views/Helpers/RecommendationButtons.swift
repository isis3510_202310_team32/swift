//
//  RecommendationButtons.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 9/03/23.
//

import SwiftUI

struct RecommendationButtons: View {
    @EnvironmentObject var networkManager: NetworkManager
    @ObservedObject var alertViewModel = AlertViewModel()
    var orientation: UIDeviceOrientation
    @Environment(\.colorScheme) var colorScheme
    @ObservedObject var recommendationVM: RecommendationsViewModel
    let locationManager = LocationManager.shared
    @EnvironmentObject var loginVM: LoginViewModel
    
    @State private var shouldShowAlert = false
    
    var body: some View {
        HStack(alignment: .center) {
            VStack {
                HStack {
                    if networkManager.isConnected && locationManager.checkLocation().0 {
                        BigIconButton(icon: "mappin.and.ellipse", text: LocalizationCacheManager.shared.getLocalized(forKey: "Location"), action: { recommendationVM.changeView(newView: .closestRestaurants)
                            recommendationVM.sendLastUse(email: (loginVM.user?.email!)!)
                        })
                    } else {
                        if !networkManager.isConnected && LocationManager.shared.locationConnection {
                            BigIconButton(icon: "mappin.and.ellipse", text: LocalizationCacheManager.shared.getLocalized(forKey: "Location"), action: {
                                alertViewModel.showAlertView(alertTitle: LocalizationCacheManager.shared.getLocalized(forKey: "Ups, no internet connection"), alertMessage: LocalizationCacheManager.shared.getLocalized(forKey: "The location feature can only be used with an active internet connection. Connect to the internet and try again later"))
                            }, color: Color.gray)
                        } else if networkManager.isConnected && !locationManager.checkLocation().0 {
                            BigIconButton(icon: "mappin.and.ellipse", text: LocalizationCacheManager.shared.getLocalized(forKey: "Location"), action: {
                                alertViewModel.showAlertView(alertTitle: LocalizationCacheManager.shared.getLocalized(forKey: "Ups, the location is off"), alertMessage: LocalizationCacheManager.shared.getLocalized(forKey: locationManager.checkLocation().1))
                            }, color: Color.gray)
                        } else {
                            BigIconButton(icon: "mappin.and.ellipse", text: LocalizationCacheManager.shared.getLocalized(forKey: "Location"), action: {
                                alertViewModel.showAlertView(alertTitle: LocalizationCacheManager.shared.getLocalized(forKey: "Ups, the location and internet are off"), alertMessage: LocalizationCacheManager.shared.getLocalized(forKey: "The location feature can only be used with an active internet connection and an active location. Connect to the internet and turn on the location and try again later"))
                                
                            }, color: Color.gray)
                        }
                    }
                    if networkManager.isConnected {
                        BigIconButton(icon: "heart.fill", text: LocalizationCacheManager.shared.getLocalized(forKey: "Preferences"), action: {
                            recommendationVM.changeView(newView: .preferences)
                            recommendationVM.sendLastUse(email: (loginVM.user?.email!)!)
                        })
                    } else {
                        BigIconButton(icon: "heart.fill", text: LocalizationCacheManager.shared.getLocalized(forKey: "Preferences"), action: {
                            alertViewModel.showAlertView(alertTitle: LocalizationCacheManager.shared.getLocalized(forKey: "Ups, no internet connection"), alertMessage: LocalizationCacheManager.shared.getLocalized(forKey: "The recomendation system can only be used with an active internet connection. Connect to the internet and try again later"))
                            
                        }, color: Color.gray)
                    }
                }
                
                HStack {
                    BigIconButton(icon: "person.2.fill", text: LocalizationCacheManager.shared.getLocalized(forKey: "Group"), action: { alertViewModel.showAlertView(alertTitle: nil, alertMessage: nil) })
                    BigIconButton(icon: "arrow.left", text: LocalizationCacheManager.shared.getLocalized(forKey: "Favourites"), action: { recommendationVM.changeView(newView: .favouriteRestaurants)})
                }
                HStack {
                    Button { alertViewModel.showAlertView(alertTitle: nil, alertMessage: nil) } label: {
                        HStack {
                            Spacer()
                            Text(LocalizationCacheManager.shared.getLocalized(forKey: "Wish me luck"))
                            Spacer()
                        }
                    }
                    .buttonStyle(.borderedProminent)
                    .foregroundColor(colorScheme == .dark ?.black : .white)
                }
            }
        }
        .onAppear {
            locationManager.checkAuthorizationStatus()
        }
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification)) { _ in
            DispatchQueue.global(qos: .background).async {
                locationManager.checkAuthorizationStatus { _ in
                    DispatchQueue.main.async {
                        recommendationVM.changeView(newView: .recommendedRestaurants)
                    }
                }
            }
        }
        .alert(isPresented: $alertViewModel.showAlert,
               content: { alertViewModel.alert() })
    }
}

struct RecommendationButtons_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            RecommendationButtons(orientation: .portrait, recommendationVM: RecommendationsViewModel())
            RecommendationButtons(orientation: .landscapeLeft, recommendationVM: RecommendationsViewModel()).previewInterfaceOrientation(.landscapeLeft)
        }.environmentObject(NetworkManager())
    }
}
