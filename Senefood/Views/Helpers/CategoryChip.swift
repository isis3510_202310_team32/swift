//
//  CategoryChip.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 5/03/23.
//

import SwiftUI

struct CategoryChip: Identifiable, View {
    var id = UUID().uuidString

    let category: String
    @Binding var isSelected: Bool

    var body: some View {
        Toggle(isOn: $isSelected) {
            Label(category.capitalized, systemImage: "")
                .symbolVariant(.rectangle)
                .labelStyle(.titleOnly)
        }
        .toggleStyle(.button)
        .background(isSelected ? Color.accentColor : Color.white)
        .overlay(
            RoundedRectangle(cornerRadius: 15)
                .stroke(isSelected ? .white : .black))
        .foregroundColor(isSelected ? .white : .black)
        .cornerRadius(15)
    }
}
