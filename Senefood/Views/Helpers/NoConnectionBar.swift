//
//  NoConnectionBar.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 12/04/23.
//

import SwiftUI

struct NoConnectionBar: View {
    @EnvironmentObject var networkManager: NetworkManager

    var body: some View {
        ZStack {
            Color.accentColor
            Text(LocalizationCacheManager.shared.getLocalized(forKey: "Senefood offline"))
                .font(.body)
                .foregroundColor(.white)
        }
        .frame(height: 25)
        .animation(.easeInOut, value: networkManager.isConnected)
    }
}

struct NoConnectionBar_Previews: PreviewProvider {
    static var previews: some View {
        NoConnectionBar()
            .environmentObject(NetworkManager())
    }
}
