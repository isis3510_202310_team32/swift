//
//  CategorySelector.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 5/03/23.
//

import SwiftUI

struct CategorySelector: View {
    @Binding var categories: [CategoryChipAttributes]
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            LazyHStack {
                ForEach(categories.indices, id: \.self) { index in
                    CategoryChip(category: categories[index].category, isSelected: $categories[index].isSelected)
                }
            }
        }
    }
}

struct CategorySelector_Previews: PreviewProvider {
    static var previews: some View {
        CategorySelector(categories: .constant(retrieveRestaurantCategories()))
    }
}
