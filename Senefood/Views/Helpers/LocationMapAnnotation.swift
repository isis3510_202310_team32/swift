//
//  LocationMapAnnotation.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 5/03/23.
//

import SwiftUI

struct LocationMapAnnotation: View {
    var body: some View {
        VStack(spacing:0) {
            Image(systemName: "fork.knife.circle.fill")
                .resizable()
                .scaledToFit()
                .frame(width: 30, height: 30)
                .foregroundColor(Color.accentColor)
            
            Image(systemName: "triangle.fill")
                .resizable()
                .scaledToFill()
                .foregroundColor(Color.accentColor)
                .frame(width: 10, height: 10)
                .rotationEffect(Angle(degrees: 180))
                .offset(y: -3)
                .padding(.bottom, 10)
        }
    }
}

struct LocationMapAnnotation_Previews: PreviewProvider {
    static var previews: some View {
        LocationMapAnnotation()
    }
}
