//
//  SortSelector.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 5/03/23.
//

import SwiftUI

enum SortingOptions: String, CaseIterable, Identifiable {
    case price, reviews, cleanness, time
    var id: Self { self }
}

struct SortSelector: View {
    @Binding var selectedOption: SortingOptions
    
    var body: some View {
        VStack(alignment: .leading) {
            Picker("Sort by", selection: $selectedOption) {
                ForEach(SortingOptions.allCases) { option in
                    Text(option.rawValue.capitalized)
                }
            }
            .pickerStyle(.wheel)
        }
    }
}

struct SortSelector_Previews: PreviewProvider {
    static var previews: some View {
        SortSelector(selectedOption: .constant(SortingOptions.price))
    }
}
