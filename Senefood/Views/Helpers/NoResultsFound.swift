//
//  NoResultsFoundBanner.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 23/04/23.
//

import SwiftUI

struct NoResultsFound: View {
    let message: String
    let size = 30.0
    let searching: Bool
    var body: some View {
        VStack(alignment: .center) {
            if searching {
                Image(systemName: "magnifyingglass")
                    .resizable()
                    .frame(width: size, height: size)
            }
            Text(message)
        }
    }
}

struct NoResultsFound_Previews: PreviewProvider {
    static var previews: some View {
        NoResultsFound(message: LocalizationCacheManager.shared.getLocalized(forKey: "Ups, no results found for your search"), searching: true)
    }
}
