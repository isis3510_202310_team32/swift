//
//  PriceRangeInput.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 5/03/23.
//

import SwiftUI

struct PriceRangeInput: View {
    @Binding var min: Double?
    @Binding var max: Double?
    @FocusState private var focusItem: Bool
    var body: some View {
        HStack {
            Text("$")
            TextField("Min", value: $min, format: .number)
                .textFieldStyle(.roundedBorder)
                .focused($focusItem, equals: true)
            Text("-")
            TextField("Max", value: $max, format: .number)
                .textFieldStyle(.roundedBorder)
                .focused($focusItem, equals: true)
            if focusItem {
                Button {
                    focusItem = false
                } label: {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Done"))
                }
            } else {
                Text("COP")
            }
        }
        .keyboardType(.numberPad)
        .onTapGesture {
            focusItem = false
        }
    }
}

struct PriceRangeInput_Previews: PreviewProvider {
    static var previews: some View {
        PriceRangeInput(min: .constant(nil), max: .constant(nil))
    }
}
