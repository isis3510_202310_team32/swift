//
//  ReviewStarsView.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 3/03/23.
//

import SwiftUI

struct ReviewStarsView: View {
    @Binding var stars: Int
    var maximumRating = 5

    var offImage = Image(systemName: "star")
    var onImage = Image(systemName: "star.fill")

    var offColor = Color.gray
    var onColor = Color.yellow

    func image(for number: Int) -> Image {
        if number > stars {
            return offImage
        } else {
            return onImage
        }
    }

    var body: some View {
        HStack(spacing: 2) {
            ForEach(1 ..< maximumRating + 1, id: \.self) { number in
                image(for: number)
                    .foregroundColor(number > stars ? offColor : onColor)
            }
        }
    }
}

struct ReviewStarsView_Previews: PreviewProvider {
    static var previews: some View {
        ReviewStarsView(stars: .constant(3))
    }
}
