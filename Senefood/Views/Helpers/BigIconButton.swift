//
//  BigIconButton.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 10/03/23.
//

import SwiftUI

struct BigIconButton: View {
    @State var size: CGSize = .zero
    let maxHeight = 90.0
    let maxWidth = 350.0
    let minWidth = 150.0

    var icon: String
    var text: String
    var action: () -> Void
    var color: Color?
    var body: some View {
        Button {
            action()
        } label: {
            ZStack {
                GeometryReader { proxy in
                    HStack(spacing: 0) {
                        Text(text)
                            .multilineTextAlignment(.leading)
                            .padding([.top, .leading], 5)
                            .foregroundColor(Color("BigButtonColor"))
                    }

                    .onAppear {
                        size = proxy.size
                    }
                }

                Image(systemName: icon)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(height: maxHeight - 20)
                    .position(x: size.width - 10, y: maxHeight / 2)
                    .foregroundColor(Color("BigButtonColor"))
            }
            .frame(minWidth: minWidth, maxWidth: maxWidth, maxHeight: maxHeight)
        }
        .buttonStyle(BackgroundButtonStyle(
            backgroundColor: color != nil ? color! : .accentColor,
            foregroundColor: Color("BigButtonColor")
        ))
    }
}

struct BackgroundButtonStyle: ButtonStyle {
    var backgroundColor: Color
    var foregroundColor: Color

    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding(10)
            .background(backgroundColor)
            .foregroundColor(foregroundColor)
            .cornerRadius(20)
    }
}

struct BigIconButton_Previews: PreviewProvider {
    static var previews: some View {
        BigIconButton(icon: "mappin.and.ellipse", text: "Preferences", action: {})
    }
}
