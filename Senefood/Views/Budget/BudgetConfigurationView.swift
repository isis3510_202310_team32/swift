//
//  BudgetConfigurationView.swift
//  Senefood
//
//  Created by Daniel Rincon on 6/03/23.
//

import SwiftUI

struct BudgetConfigurationView: View {
    @Environment(\.dismiss) var dismiss
    @EnvironmentObject var networkManager: NetworkManager
    @ObservedObject var alertViewModel = AlertViewModel()
    @ObservedObject var budgetViewModel: BudgetViewModel
    
    var body: some View {
        VStack(alignment: .center) {
            FormBudgetConfigurationView(budgetViewModel: budgetViewModel)
            
            if budgetViewModel.isBudgetConfigured() && budgetViewModel.isBudgetFormValid() {
                Text(LocalizationCacheManager.shared.getLocalized(forKey: "Confirming the new budget configuration will overwrite the current budget recommendations!"))
                    .padding()
                    .foregroundColor(Color.accentColor)
            }
            
            HStack(alignment: .center, spacing: 30.0) {
                Button(
                    action: {
                        resetBudget()
                    },
                    label: {
                        Text(LocalizationCacheManager.shared.getLocalized(forKey: "Reset"))
                            .foregroundColor(.white)
                            .frame(width: 120.0)
                    })
                .background(.black)
                .cornerRadius(10)
                .controlSize(.large)
                
                Button(
                    action: {
                        createBudget()
                    },
                    label: {
                        Text(LocalizationCacheManager.shared.getLocalized(forKey: "Done"))
                            .foregroundColor(.black)
                            .frame(width: 120.0)
                            .background(.clear)
                    })
                .disabled(!budgetViewModel.isBudgetFormValid())
                .background(budgetViewModel.isBudgetFormValid() ? Color("SecondaryColor") : .gray)
                .cornerRadius(10)
                .controlSize(.large)
            }
            .padding(.bottom)
            .buttonStyle(.bordered)
        }
        .alert(isPresented: $alertViewModel.showAlert,
               content: { alertViewModel.alert() })
    }
    
    func resetBudget() {
        budgetViewModel.resetConfigureBudgetForm()
    }
    
    func createBudget() {
        if networkManager.isConnected {
            budgetViewModel.configureBudget()
            dismiss()
        } else {
            alertViewModel.showAlertView(alertTitle: LocalizationCacheManager.shared.getLocalized(forKey: "Ups, no internet connection"), alertMessage: LocalizationCacheManager.shared.getLocalized(forKey: "Budget cannot be computed without internet connection"))
        }
    }
}
struct BudgetConfigurationView_Previews: PreviewProvider {
    static var previews: some View {
        BudgetConfigurationView(budgetViewModel: BudgetViewModel())
    }
}
