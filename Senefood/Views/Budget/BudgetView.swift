//
//  BudgetView.swift
//  Senefood
//
//  Created by Daniel Rincon on 6/03/23.
//

import SwiftUI

struct BudgetView: View {
    @ObservedObject var budgetViewModel : BudgetViewModel
    
    var body: some View {
        NavigationStack {
            // View stack
            ScrollView {
                if budgetViewModel.errorCreatingBudget {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "We could not find any combination of dishes that can satisfy your budget-saving plan in the given days. Try another alternative!"))
                        .padding()
                        .foregroundColor(Color.accentColor)
                }
                if budgetViewModel.creatingBudgetInProgress {
                    ProgressView()
                        .scaleEffect(2)
                        .padding()
                }
                if budgetViewModel.isBudgetConfigured() {
                    VStack(alignment: .leading) {
                        Text(LocalizationCacheManager.shared.getLocalized(forKey: "Daily Spendings"))
                            .font(.title2)
                            .fontWeight(.bold)
                            .padding([.top, .leading, .trailing])
                        // Information per day stack
                        DailyBudgetView(budgetViewModel: budgetViewModel)
                        
                        
                        Text(LocalizationCacheManager.shared.getLocalized(forKey: "How much are you saving?"))
                            .font(.title2)
                            .fontWeight(.bold)
                            .padding([.top, .leading, .trailing])
                        Text(LocalizationCacheManager.shared.getLocalized(forKey: "Weekly Budget") + ": " + budgetViewModel.numberToMoney(number: Int(budgetViewModel.currentBudget[0].budget)) + " COP")
                            .padding([.leading, .trailing])
                        Text(LocalizationCacheManager.shared.getLocalized(forKey: "Savings") + ": \(budgetViewModel.getSavingsBudget() > 0 ? budgetViewModel.numberToMoney(number: budgetViewModel.getSavingsBudget()) : "$0") COP")
                            .fontWeight(.bold)
                            .padding([.leading, .trailing])
                    }
                    HStack (alignment: .center) {
                        NavigationLink(destination: BudgetDetailList(budgetDays: budgetViewModel.currentBudget[0].daysArray)) {
                            Text(LocalizationCacheManager.shared.getLocalized(forKey: "View Dishes"))
                                .padding(10)
                                .font(.body)
                                .background(Color.accentColor)
                                .foregroundColor(.white)
                                .cornerRadius(10)
                        }
                    }
                    .padding()
                } else {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Have you configured your weekly budget yet?"))
                        .font(.title2)
                        .fontWeight(.bold)
                        .padding([.top, .leading, .trailing])
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "no_budget_conf"))
                        .padding()
                }
                
                HStack(alignment: .center){
                    Spacer()
                    
                    Button(action: {
                        budgetViewModel.errorCreatingBudget = false
                        budgetViewModel.showingBudgetConfigurationSheet.toggle()
                    }) {
                        Text(LocalizationCacheManager.shared.getLocalized(forKey: "Configure Budget"))
                            .foregroundColor(.black)
                            .background(.clear)
                    }
                    .background(Color("SecondaryColor"))
                    .cornerRadius(10)
                    .controlSize(.large)
                    .sheet(isPresented: $budgetViewModel.showingBudgetConfigurationSheet) {
                        BudgetConfigurationView(budgetViewModel: budgetViewModel)
                    }
                    
                    Spacer()
                }.buttonStyle(.bordered)
                
            }
            .navigationTitle(LocalizationCacheManager.shared.getLocalized(forKey: "Budget"))
            .navigationBarTitleDisplayMode(.inline)
        }
        
    }
}

struct BudgetView_Previews: PreviewProvider {
    static var previews: some View {
        BudgetView(budgetViewModel: BudgetViewModel())
    }
}
