//
//  BudgetDetailList.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 24/05/23.
//

import SwiftUI

struct BudgetDetailList: View {
    var budgetDays : [BudgetDay]
    
    var body: some View {
        NavigationView {
            List {
                ForEach(budgetDays) { day in
                    Section(header: Text(LocalizationCacheManager.shared.getLocalized(forKey: day.fullName ?? "Day"))) {
                        ForEach(day.dishesArray) { dish in
                            BudgetDishRow(dish: dish)
                        }
                    }
                }
            }
            .listStyle(.plain)
            .headerProminence(.increased)
            .navigationTitle(LocalizationCacheManager.shared.getLocalized(forKey: "Detail Budget"))
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

struct BudgetDetailList_Previews: PreviewProvider {
    static var previews: some View {
        BudgetDetailList(budgetDays: [])
    }
}
