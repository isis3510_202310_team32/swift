//
//  FormBudgetView.swift
//  Senefood
//
//  Created by Daniel Rincon on 6/03/23.
//

import SwiftUI

struct FormBudgetConfigurationView: View {
    @ObservedObject var budgetViewModel: BudgetViewModel
    @FocusState private var focusItem: Bool
    
    var body: some View {
        Form {
            // Budget value and savings
            Section(header: Text(LocalizationCacheManager.shared.getLocalized(forKey: "Weekly Budget")), footer: !budgetViewModel.budgetNumberError ? Text(LocalizationCacheManager.shared.getLocalized(forKey: "Percentage of the total budget saved")) : Text(LocalizationCacheManager.shared.getLocalized(forKey: "Number too large")).foregroundColor(.accentColor)) {
                
                HStack {
                    TextField("$$$", value: $budgetViewModel.budget, format: .number)
                        .textFieldStyle(.roundedBorder)
                        .focused($focusItem, equals: true)
                        .keyboardType(.numberPad)
                    if focusItem {
                        Button {
                            focusItem = false
                        } label: {
                            Text(LocalizationCacheManager.shared.getLocalized(forKey: "Done"))
                        }
                    } else {
                        Text("COP")
                    }
                }
                
                Slider(
                    value: $budgetViewModel.savings,
                    in: 0 ... 100,
                    step: 5
                ) {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Saving"))
                } minimumValueLabel: {
                    Text("0")
                } maximumValueLabel: {
                    Text("\(Int(budgetViewModel.savings))%")
                }
                .accentColor(Color("AccentColor"))
            }
            // Budget days
            Section(header: Text(LocalizationCacheManager.shared.getLocalized(forKey: "Days of the Week"))) {
                Toggle(isOn: $budgetViewModel.selectedWeekdays[0]) {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Monday"))
                }.tint(Color("SecondaryColor"))
                Toggle(isOn: $budgetViewModel.selectedWeekdays[1]) {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Tuesday"))
                }.tint(Color("SecondaryColor"))
                Toggle(isOn: $budgetViewModel.selectedWeekdays[2]) {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Wednesday"))
                }.tint(Color("SecondaryColor"))
                Toggle(isOn: $budgetViewModel.selectedWeekdays[3]) {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Thursday"))
                }.tint(Color("SecondaryColor"))
                Toggle(isOn: $budgetViewModel.selectedWeekdays[4]) {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Friday"))
                }.tint(Color("SecondaryColor"))
            }
        }
    }
}


struct FormBudgetView_Previews: PreviewProvider {
    static var previews: some View {
        FormBudgetConfigurationView(budgetViewModel: BudgetViewModel())
    }
}
