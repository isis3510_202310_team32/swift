//
//  DailyBudgetView.swift
//  Senefood
//
//  Created by Daniel Rincon on 6/03/23.
//

import SwiftUI

struct DailyBudgetView: View {
    @ObservedObject var budgetViewModel: BudgetViewModel
    @State private var containerWidth: CGFloat = 0
    
    private let maxBudget = 150000
    
    var body: some View {
        VStack(alignment: .leading) {
            ForEach(budgetViewModel.currentBudget[0].daysArray) { day in
                HStack {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: day.shortName ?? "Mon"))
                        .frame(width: 50.0)
                    
                    ZStack(alignment: .leading) {
                        GeometryReader { geo in
                            RoundedRectangle(cornerRadius: 60)
                                .foregroundColor(.clear)
                                .onAppear {
                                    containerWidth = geo.size.width * 8/9
                                }
                        }
                        
                        ZStack(alignment: .trailing) {
                            RoundedRectangle(cornerRadius: 60)
                                .fill(Color("AccentColor"))
                            // Text("$"+String(spentPerDay[day]/1000)+".000")
                            Text(budgetViewModel.numberToMoney(number: day.dishesArray.reduce(0) { $0 + Int($1.price) }))
                                .foregroundColor(.white)
                                .padding(EdgeInsets(top: 6, leading: 12, bottom: 6, trailing: 12))
                                .background(
                                    RoundedRectangle(cornerRadius: 60)
                                        .fill(Color("AccentColor"))
                                )
                        }
                        .padding(2)
                        .frame(minWidth: min(CGFloat(day.dishesArray.reduce(0) { $0 + $1.price }) * containerWidth / CGFloat(budgetViewModel.computeMaxDayCost()), containerWidth))
                        .fixedSize()
                    }
                    .fixedSize(horizontal: false, vertical: true)
                    .padding(1)
                }
                Divider()
            }
        }
    }
}

struct DailyBudgetView_Previews: PreviewProvider {
    static var previews: some View {
        DailyBudgetView(budgetViewModel: BudgetViewModel())
    }
}
