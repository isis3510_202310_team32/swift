//
//  BudgetDishRow.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 24/05/23.
//

import SwiftUI
import Kingfisher

struct BudgetDishRow: View {
    var dish: BudgetDish?
    
    var body: some View {
        HStack {
            KFImage(URL(string: dish?.imageURL ?? "https://picsum.photos/id/63/400/400"))
                .placeholder { Image("logo").resizable().frame(width: 125, height: 125) }
                .loadDiskFileSynchronously()
                .cacheMemoryOnly()
                .fade(duration: 0.25)
                .resizable()
                .frame(width: 125, height: 125)
            Spacer()
            
            VStack(alignment: .leading) {
                Text(dish?.name ?? "Placeholder Name")
                    .font(.title2)
                
                Text(dish?.restaurant ?? "Restaurant")
                    .font(.title3)
                
                Text(calculatePrice() + " COP")
                    .fontWeight(.semibold)
            }.frame(
                minWidth: 0,
                maxWidth: .infinity,
                alignment: .topLeading
            )
        }
    }
    
    func calculatePrice() -> String {
        var numberString = String(Int(dish?.price ?? 10000) )
        let hundreth = numberString.suffix(3)
        
        let index = numberString.index(numberString.endIndex, offsetBy: -3)
        let thousand = numberString[..<index]
        
        numberString = "$" + String(thousand) + "." + String(hundreth)
        return numberString
    }
}

struct BudgetDishRow_Previews: PreviewProvider {
    static var previews: some View {
        BudgetDishRow(dish: nil)
    }
}
