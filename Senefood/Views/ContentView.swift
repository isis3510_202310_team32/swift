//
//  ContentView.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 3/03/23.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var loginViewModel: LoginViewModel
    @EnvironmentObject var networkManager: NetworkManager
    @StateObject var restaurantHostViewModel = RestaurantHostViewModel()
    @StateObject var budgetViewModel = BudgetViewModel()
    @StateObject var recommendationVM = RecommendationsViewModel()
    @StateObject var closestRestaurantVM = ClosestRestaurantsViewModel()
    @StateObject var favouriteVM = FavouriteViewModel()
    @StateObject private var diaryVM = DiaryViewModel()
    let eventCaller = EventManagerViewModel.instance

    var body: some View {
        Group {
            if let user = loginViewModel.user {
                if loginViewModel.firstTimeUser {
                    FirstTimeUserView(user: user, loginVM: loginViewModel)
                } else {
                    VStack(spacing: 0) {
                        TabView {
                            RestaurantHost()
                                .environmentObject(restaurantHostViewModel)
                                .environmentObject(favouriteVM)
                                .tabItem {
                                    Label(LocalizationCacheManager.shared.getLocalized(forKey: "Restaurants"), systemImage: "fork.knife")
                                }
                            BudgetView(budgetViewModel: budgetViewModel)
                                .tabItem {
                                    Label(LocalizationCacheManager.shared.getLocalized(forKey: "Budget"), systemImage: "dollarsign.circle")
                                }

                            RecommendationView(recommendationVM: recommendationVM, loginModel: loginViewModel, closestRestaurantVM: closestRestaurantVM)
                                .environmentObject(restaurantHostViewModel)
                                .environmentObject(favouriteVM)
                                .tabItem {
                                    Label(LocalizationCacheManager.shared.getLocalized(forKey: "Recommend"), systemImage: "sparkles")
                                }
                            UserSummary(loginVM: loginViewModel)
                                .tabItem {
                                    Label(LocalizationCacheManager.shared.getLocalized(forKey: "Profile"), systemImage: "person.crop.circle")
                                }
                            DiaryMainView(diaryVM: diaryVM, currentUserEmail: user.email!)
                                .tabItem {
                                    Label(LocalizationCacheManager.shared.getLocalized(forKey: "Diary"), systemImage: "book.fill")
                                }
                        }
                        if !networkManager.isConnected {
                            NoConnectionBar()
                        }
                    }
                }
            } else {
                LoginView(loginVM: loginViewModel)
            }

        }.onAppear {
            loginViewModel.listenToAuthState()
            eventCaller.sendEventTimeOfLaunch()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(LoginViewModel())
            .environmentObject(NetworkManager())
    }
}
