//
//  BudgetView.swift
//  Senefood
//
//  Created by Daniel Rincon on 6/03/23.
//

import SwiftUI

struct BudgetView: View {
    @Environment(\.verticalSizeClass) var verticalSizeClass
    
    var body: some View {
        NavigationView{
            GeometryReader{ geometry in
                // View stack
                ScrollView(){
                    VStack(alignment: .leading){
                        Text("Gastos diarios")
                            .font(.title)
                            .padding([.top, .leading, .trailing])
                        // Information per day stack
                        DailyBudgetView()
                        
                        
                        Text("¿Cuánto te has ahorrado?")
                            .font(.title)
                            .padding([.top, .leading, .trailing])
                        Text("Basado en tu presupuesto semanal de $100.000 COP, te estarás ahorrando $15.000 COP")
                            .padding()
                    }
                    
                    HStack(alignment: .center){
                        
                        Spacer()
                        
                        NavigationLink(
                            destination: BudgetConfigurationView(),
                            label: {
                                Text("Configure Budget")
                                    .foregroundColor(.black)
                                    .frame(width: 150.0, height: 20.0)
                                    .background(.clear)
                            })
                        .background(Color("SecondaryColor"))
                        .cornerRadius(10)
                        .controlSize(.large)
                        
                        Spacer()
                    }.buttonStyle(.bordered)
                    
                }
                .navigationTitle("Budget")
                .navigationBarTitleDisplayMode(.inline)
                .frame(height: geometry.size.height)
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationViewStyle(StackNavigationViewStyle())
        
    }
}

struct BudgetView_Previews: PreviewProvider {
    static var previews: some View {
        BudgetView()
    }
}
