//
//  ReviewsView.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 1/04/23.
//

import FirebaseFirestore
import SwiftUI

struct ReviewsView: View {
    var restaurant: Restaurant

    var body: some View {
        VStack {
            List {
                ForEach(restaurant.reviews) { review in
                    ReviewRow(review: review)
                }
            }.listStyle(.plain)
        }.navigationTitle(LocalizationCacheManager.shared.getLocalized(forKey: "Reviews"))
            .navigationBarItems(trailing: NavigationLink(destination: AddReviewView(restaurant: restaurant)) {
                Image(systemName: "plus")
            })
    }
}

struct ReviewsView_Previews: PreviewProvider {
    static var previews: some View {
        ReviewsView(restaurant: Restaurant(name: "Toninos", stars: 5, cleannessScore: 10, categories: ["Italian", "Pasta"], dishes: [], imageName: "", coordinates: GeoPoint(latitude: 0, longitude: 0), recommendationScore: 0, reviews: [Review(id: "1", user: "Pepito Pérez", stars: 5, text: "The experience was awesome. The dish I ordered was great and I loved how it tasted. I will be back soon")]))
    }
}
