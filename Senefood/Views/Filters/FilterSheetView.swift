//
//  FilterSheetView.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 5/03/23.
//

import SwiftUI

struct FilterSheetView: View {
    @Environment(\.dismiss) var dismiss
    
    @ObservedObject var restaurantHostViewModel: RestaurantHostViewModel
    
    var body: some View {
        VStack {
            HStack {
                Label(LocalizationCacheManager.shared.getLocalized(forKey: "Filters"), systemImage: "line.3.horizontal.decrease.circle")
                    .font(.largeTitle)
                
                Spacer()
                
                Button(action: dismissSheet, label: {
                    ZStack {
                        Circle()
                            .fill(Color.gray.opacity(0.1))
                            .frame(width: 30, height: 30)
                        
                        Image(systemName: "xmark")
                            .font(.system(size: 15, weight: .bold, design: .rounded))
                            .foregroundColor(.secondary)
                    }
                    .padding(8)
                    .contentShape(Circle())
                })
                .buttonStyle(PlainButtonStyle())
                .accessibilityLabel(Text("Close"))
            }
            Spacer()
            ScrollView {
                VStack(alignment: .leading) {
                    Group {
                        Group {
                            HStack {
                                Text(LocalizationCacheManager.shared.getLocalized(forKey: "Price"))
                                    .font(.title2)
                                if restaurantHostViewModel.minmaxNumberError {
                                    Text("Number too large")
                                        .fontWeight(.light)
                                        .foregroundColor(.accentColor)
                                }
                            }
                            
                            PriceRangeInput(min: $restaurantHostViewModel.min, max: $restaurantHostViewModel.max)
                        }
                        Group {
                            Text(LocalizationCacheManager.shared.getLocalized(forKey: "Category"))
                                .font(.title2)
                            ScrollView(.horizontal, showsIndicators: false) {
                                LazyHStack {
                                    ForEach(restaurantHostViewModel.categories.indices, id: \.self) { index in
                                        CategoryChip(category: restaurantHostViewModel.categories[index].category, isSelected: $restaurantHostViewModel.categories[index].isSelected)
                                    }
                                }
                            }
                            
                        }
                        Group {
                            Text(LocalizationCacheManager.shared.getLocalized(forKey: "Sort by:"))
                                .font(.title)
                            Picker("Sort by", selection: $restaurantHostViewModel.sortBy) {
                                ForEach(SortingOptions.allCases) { option in
                                    Text(NSLocalizedString(option.rawValue.capitalized, comment: ""))
                                }
                            }
                            
                            .pickerStyle(.wheel)
                        }
                    }.padding([.bottom])
                    
                    if restaurantHostViewModel.lastFilterCacheActive() {
                        Button {
                            recoverLastFilter()
                        } label: {
                            Text(LocalizationCacheManager.shared.getLocalized(forKey: "Use Previous Filter"))
                                .frame(maxWidth: .infinity, maxHeight: 50)
                        }
                        .buttonStyle(.borderedProminent)
                        .font(.title2)
                        .tint(Color("AccentColor"))
                        .foregroundColor(.white)
                    }
                }
                HStack {
                    Group {
                        Button {
                            resetFilters()
                        } label: {
                            Text(LocalizationCacheManager.shared.getLocalized(forKey: "Reset"))
                                .frame(maxWidth: .infinity, maxHeight: 50)
                        }
                        .tint(.black)
                        Button {
                            applyFilters()
                        } label: {
                            Text(LocalizationCacheManager.shared.getLocalized(forKey: "Done"))
                                .frame(maxWidth: .infinity, maxHeight: 50)
                        }
                        .tint(Color("SecondaryColor"))
                        .foregroundColor(.black)
                    }
                    .buttonStyle(.borderedProminent)
                    .font(.title2)
                }.frame(
                    minWidth: 0,
                    maxWidth: .infinity
                )
            }
        }
        .padding()
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
    
    func dismissSheet() {
        dismiss()
    }
    
    func resetFilters() {
        restaurantHostViewModel.min = nil
        restaurantHostViewModel.max = nil
        restaurantHostViewModel.sortBy = SortingOptions.price
        restaurantHostViewModel.categories = retrieveRestaurantCategories()
        restaurantHostViewModel.eraseCacheLastFilter()
        restaurantHostViewModel.search()
        dismiss()
    }
    
    func applyFilters() {
        restaurantHostViewModel.cacheLastFilter()
        restaurantHostViewModel.search()
        dismiss()
    }
    
    func recoverLastFilter() {
        restaurantHostViewModel.loadLastFilterCache()
    }
}

struct FilterSheetView_Previews: PreviewProvider {
    static var previews: some View {
        FilterSheetView(restaurantHostViewModel: RestaurantHostViewModel())
    }
}
