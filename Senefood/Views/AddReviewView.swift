//
//  AddReviewView.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 1/04/23.
//

import FirebaseFirestore
import SwiftUI

struct AddReviewView: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var reviewsViewModel = ReviewsViewModel()
    @EnvironmentObject var loginViewModel: LoginViewModel
    @EnvironmentObject var networkManager: NetworkManager
    @ObservedObject var alertViewModel = AlertViewModel()
    let eventCaller = EventManagerViewModel.instance

    var restaurant: Restaurant

    var body: some View {
        ScrollView {
            VStack {
                Text(LocalizationCacheManager.shared.getLocalized(forKey: reviewsViewModel.localStorageMessage))
                    .font(.subheadline)
                    .padding()
                
                HStack {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Rating:"))
                    ForEach(1 ..< 6) { number in
                        Image(systemName: number <= reviewsViewModel.starRating ? "star.fill" : "star")
                            .foregroundColor(.yellow)
                            .onTapGesture {
                                reviewsViewModel.starRating = number
                            }
                    }
                }
                TextField(LocalizationCacheManager.shared.getLocalized(forKey: "Input your review"), text: $reviewsViewModel.reviewText, axis: .vertical)
                    .textFieldStyle(.roundedBorder)
                    .padding()

                Text("\(reviewsViewModel.reviewText.count)/150").multilineTextAlignment(.leading)
                    .font(.caption)
                    .foregroundColor(Color(.placeholderText))
                    .offset(y: -20)

                Button(
                    action: {
                        if networkManager.isConnected {
                            reviewsViewModel.submitReview(restaurantId: restaurant.id!, stars: reviewsViewModel.starRating, text: reviewsViewModel.reviewText, user: loginViewModel.user!.displayName!)
                            eventCaller.sendEventRestaurantRatingReview(restaurant: restaurant, rating: reviewsViewModel.starRating)
                            presentationMode.wrappedValue.dismiss()
                            reviewsViewModel.setDefaultValues()
                        } else {
                            alertViewModel.showAlertView(alertTitle: LocalizationCacheManager.shared.getLocalized(forKey: "Ups, no internet connection"), alertMessage: LocalizationCacheManager.shared.getLocalized(forKey: "Review cannot be sent without internet connection. Content will be saved"))
                        }
                    },
                    label: {
                        Text(LocalizationCacheManager.shared.getLocalized(forKey: "Submit"))
                            .frame(maxWidth: .infinity, maxHeight: 50)
                    }).buttonStyle(.borderedProminent)
                    .disabled(reviewsViewModel.reviewText.trimmingCharacters(in: .whitespacesAndNewlines) == "")
            }
            .onAppear() {
                reviewsViewModel.localStorageMessage = reviewsViewModel.defaultValues() ? "" : "Retrieved last saved review that was not sent!"
            }
            .alert(isPresented: $alertViewModel.showAlert,
                   content: { alertViewModel.alert() })
            .padding()
            .navigationTitle(LocalizationCacheManager.shared.getLocalized(forKey: "Add a review"))
        }
    }
}

struct AddReviewView_Previews: PreviewProvider {
    static var previews: some View {
        AddReviewView(restaurant: Restaurant(name: "Toninos", stars: 5, cleannessScore: 10, categories: ["Italian", "Pasta"], dishes: [], imageName: "", coordinates: GeoPoint(latitude: 0, longitude: 0), recommendationScore: 0, reviews: [Review(id: "1", user: "Pepito Pérez", stars: 5, text: "The experience was awesome. The dish I ordered was great and I loved how it tasted. I will be back soon")]))
    }
}
