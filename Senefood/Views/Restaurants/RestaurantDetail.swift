//
//  RestaurantDetail.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 7/03/23.
//

import FirebaseAnalytics
import SwiftUI
import AVFoundation


struct RestaurantDetail: View {
    @Environment(\.horizontalSizeClass) var sizeClass
    @EnvironmentObject var loginViewModel: LoginViewModel
    @EnvironmentObject var restaurantHostViewModel: RestaurantHostViewModel
    @EnvironmentObject var favouriteViewModel : FavouriteViewModel
    @State private var searchText = ""
    @State private var showingSheet = false
    @State private var isStarred = false
    
    @State var audioViewModel = AudioViewModel.instance
    
    
    private let starOnAudio = URL(string: "https://firebasestorage.googleapis.com/v0/b/senefood-2b853.appspot.com/o/sounds%2Fstar_on.mp3?alt=media&token=c4657a27-8d23-4bb1-a5d0-f4ac049dd863")
    private let starOffAudio = URL(string: "https://firebasestorage.googleapis.com/v0/b/senefood-2b853.appspot.com/o/sounds%2Fstar_off.mp3?alt=media&token=b2dcbbba-de4e-41f2-a694-b0accdffc3fc")
    
    
    var searchTextBinding: Binding<String> {
        Binding<String>(
            get: { self.searchText },
            set: { newValue in
                if newValue.count > 50 {
                    self.searchText = String(newValue.prefix(50))
                }
                else {
                    self.searchText = newValue
                }
            }
        )
    }
    
    var restaurant: Restaurant
    let eventCaller = EventManagerViewModel.instance
    var closest: Bool
    var distance: String
    
    var searchedDishes: [Dish] {
        restaurant.dishes.filter { searchText.isEmpty ? true : $0.name.lowercased().contains(searchText.lowercased())
        }
    }
    
//    private func playSound() {
//        guard let url = Bundle.main.url(forResource: "star_on", withExtension: "mp3") else {
//            print("Sound file not found")
//            return
//        }
//        do {
//            player = try AVAudioPlayer(contentsOf: url)
//            player?.play()
//        } catch {
//            print("Sound playback error: \(error)")
//        }
//    }
    
    var body: some View {
        NavigationView {
            List {
                Section {
                    RestaurantDetailCard(restaurant: restaurant,closest: closest, distance: distance)
                }
                .listRowBackground(Color.clear)
                .listRowSeparator(.hidden)
                Section(header: Text(LocalizationCacheManager.shared.getLocalized(forKey: "All dishes"))) {
                    if searchedDishes.count == 0 && searchText.count == 0 {
                        HStack {
                            Spacer()
                            NoResultsFound(message: LocalizationCacheManager.shared.getLocalized(forKey: "Couldn't load the dishes for the restaurant"), searching: false)
                            Spacer()
                        }
                    }
                    else if searchedDishes.count > 0 {
                        ForEach(searchedDishes.sorted(by: { $0.price < $1.price })) { dish in
                            DishRow(dish: dish)
                        }
                    }
                    else {
                        HStack {
                            Spacer()
                            NoResultsFound(message: LocalizationCacheManager.shared.getLocalized(forKey: "Ups, no results found for your search"), searching: true)
                            Spacer()
                        }
                    }
                }
            }
            .listStyle(.plain)
            .headerProminence(.increased)
            .navigationTitle(restaurant.name)
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button(action: {
                        isStarred.toggle()
                        if isStarred{
                            //audioViewModel.downloadFile(audioUrl: starOnAudio!)
                            audioViewModel.playAudio(audioUrl: starOnAudio!)
                            favouriteViewModel.addRestaurant(username: loginViewModel.registeredUser?.username ?? "dummyUser", restaurant: restaurant)
                        }
                        else{
                            //audioViewModel.downloadFile(audioUrl: starOffAudio!)
                            audioViewModel.playAudio(audioUrl: starOffAudio!)
                            isStarred = false
                            favouriteViewModel.removeRestaurant(restaurant: restaurant, username: loginViewModel.registeredUser?.username ?? "NO USERNAME")
                        }
                    }) {
                        Image(systemName: "star")
                            .resizable()
                            .frame(width: 24, height: 24)
                            .foregroundColor(.yellow)
                            .overlay(
                                Image(systemName: "star.fill")
                                    .resizable()
                                    .frame(width: 24, height: 24)
                                    .foregroundColor(.yellow)
                                    .opacity(isStarred ? 1 : 0)
                            ).padding(.trailing)
                    }
                }
            }
        }
        .onAppear {
            isStarred = favouriteViewModel.isFavourite(restaurant: restaurant)
            eventCaller.sendEventPerCategory(restaurant: restaurant)
            eventCaller.sendEventRestaurantPerDate(restaurant: restaurant)
            if closest == false{
                restaurantHostViewModel.addRecentlyVisitedRestaurant(loginViewModel.registeredUser?.username ?? "dummyUser", restaurant.id!)
                restaurantHostViewModel.selectedRestaurant = restaurant
            }
        }
        .searchable(text: searchTextBinding)
    }
}

struct RestaurantDetail_Previews: PreviewProvider {
    static let restaurants = RestaurantHostViewModel().restaurants
    static var previews: some View {
        RestaurantDetail(restaurant: restaurants[0], closest: false, distance: "0")
    }
}
