//
//  RestaurantListView.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 4/03/23.
//

import SwiftUI

struct RestaurantHost: View {
    @EnvironmentObject var restaurantHostViewModel: RestaurantHostViewModel
    @EnvironmentObject var loginViewModel: LoginViewModel
    
    var recentlyVisitedRestaurants: [Restaurant] {
        restaurantHostViewModel.retrieveRecentlyVisitedRestaurants(for: loginViewModel.registeredUser?.username ?? "dummyUser")
    }
    
    func filterRestaurants() {
        restaurantHostViewModel.showingSheet.toggle()
    }
    
    var body: some View {
        NavigationStack {
            HStack {
                Button(action: filterRestaurants) {
                    Label(LocalizationCacheManager.shared.getLocalized(forKey: "Filter"),
                          systemImage: "line.3.horizontal.decrease.circle")
                }
                .sheet(isPresented: $restaurantHostViewModel.showingSheet) {
                    FilterSheetView(restaurantHostViewModel: restaurantHostViewModel)
                        .padding()
                }
                Spacer()
                Toggle(isOn: $restaurantHostViewModel.mapView) {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Map view"))
                        .fontWeight(.semibold)
                        .frame(maxWidth: .infinity, alignment: .trailing)
                }
                .tint(Color("SecondaryColor"))
            }.padding(.horizontal)
            
            VStack(alignment: .center) {
                if restaurantHostViewModel.filteringInProgress {
                    ProgressView()
                        .scaleEffect(2)
                        .padding()
                }
                if !restaurantHostViewModel.mapView {
                    RestaurantList(recentlyVisitedRestaurants: recentlyVisitedRestaurants, searchedRestaurants: restaurantHostViewModel.searchedRestaurants, searching: restaurantHostViewModel.searchTextActive(), filtering: restaurantHostViewModel.filtersActive())
                } else {
                    RestaurantMap(restaurants: restaurantHostViewModel.searchedRestaurants)
                }
            }
            .navigationTitle(LocalizationCacheManager.shared.getLocalized(forKey: "Restaurants"))
            .navigationBarTitleDisplayMode(.inline)
        }
        .searchable(text: $restaurantHostViewModel.searchText)
        .onAppear() {
            restaurantHostViewModel.search()
        }
    }
}

struct RestaurantHostView_Previews: PreviewProvider {
    static var loginViewModel = LoginViewModel()
    static var restaurantHostViewModel = RestaurantHostViewModel()
    
    static var previews: some View {
        RestaurantHost()
            .environmentObject(restaurantHostViewModel)
            .environmentObject(loginViewModel)
    }
}
