//
//  FavouriteRestaurantView.swift
//  Senefood
//
//  Created by Daniel Rincon on 25/05/23.
//

import SwiftUI

struct FavouriteRestaurantView: View {
    
    @ObservedObject var loginVM: LoginViewModel
    @ObservedObject var recommendationVM: RecommendationsViewModel
    @EnvironmentObject var favouriteViewModel : FavouriteViewModel
    
    @State private var deletionConfirmation = false
    @State private var itemToDelete: Restaurant?
    @State private var usedMove = false
    
    let event = EventManagerViewModel.instance
    
    var body: some View {
        NavigationView{
            if favouriteViewModel.restaurants.count > 0{
                VStack(spacing: 20){
                    List{
                        Section(header: Text(LocalizationCacheManager.shared.getLocalized(forKey: "Your favourite restaurants"))){
                            ForEach(favouriteViewModel.restaurants){ restaurant in
                                RestaurantListRow(restaurant: restaurant, closest: false, distance: "0", imageLoader: ImageCacheViewModel())
                                    .background(
                                        NavigationLink(restaurant.name,
                                                       destination: RestaurantDetail(restaurant: restaurant, closest: false, distance: "0"))
                                        .opacity(0)
                                    )
                            }
                            .onMove { from, to in
                                usedMove = true
                                favouriteViewModel.reorderRestaurants(from: from, destination: to)
                            }
                            .onDelete { offsets in
                                let index = offsets.first ?? 0
                                itemToDelete = favouriteViewModel.restaurants[index]
                                deletionConfirmation = true
                            }
                        }
                    }
                    Spacer()
                }
            } else{
                VStack{//Spacer()
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Ups! You don't have any favourite restaurnts."))
                    //Spacer()
                }
            }
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarItems(leading: Button("Back", action: {
            favouriteViewModel.movedUsed(used: usedMove)
            recommendationVM.changeView(newView: .recommendedRestaurants)
        }))
        .navigationViewStyle(StackNavigationViewStyle())
        .alert(isPresented: $deletionConfirmation) {
            Alert(
                title: Text(LocalizationCacheManager.shared.getLocalized(forKey:"Confirmation")),
                message: Text(LocalizationCacheManager.shared.getLocalized(forKey: "Are you sure you want to delete this restaurant from favourites?")),
                primaryButton: .cancel(),
                secondaryButton: .destructive(Text(LocalizationCacheManager.shared.getLocalized(forKey:"Delete"))) {
                    if let restaurant = itemToDelete {
                        favouriteViewModel.removeRestaurant(restaurant: restaurant, username: loginVM.registeredUser?.username ?? "NO USERNAME")
                    }
                }
            )
        }
        .onAppear{
            favouriteViewModel.fetchAllRestaurants()
            event.sendEventFavouriteRestaurantsFeature(feature: "Favourites")
        }
    }
}
