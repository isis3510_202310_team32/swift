//
//  RestaurantDetailCard.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 7/03/23.
//

import CoreLocation
import FirebaseFirestore
import SwiftUI

struct RestaurantDetailCard: View {
    var restaurant: Restaurant
    var closest: Bool
    var distance: String
    
    @StateObject private var imageLoader = ImageCacheViewModel()
    
    private func calculatePreparationTime() -> String {
        let prepTime = Double(restaurant.dishes.map { $0.preparationTime }.reduce(0, +))/Double(restaurant.dishes.count)
        return "\(prepTime) mins"
    }
    
    private func calculatePriceRange() -> String {
        let meanCost = Double(restaurant.dishes.map { $0.price }.reduce(0, +))/Double(restaurant.dishes.count)
        var costSymbol = "?"
        switch meanCost {
        case 0...12000:
            costSymbol = "$"
        case 12000...24000:
            costSymbol = "$$"
            
        default:
            costSymbol = "$$$"
        }
        return costSymbol
    }
    
    private func calculateCleannessScore() -> String {
        return "\(restaurant.cleannessScore)/10"
    }
    
    var body: some View {
        HStack {
            
            if imageLoader.isLoading{
                Image("logo")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 150, height: 150)
            } else if let image = imageLoader.image {
                Image(uiImage: image)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 150, height: 150)
            }
            
            VStack(alignment: .leading) {
                if closest == false{
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Est. prep. time: "))
                        .fontWeight(.semibold) + Text(calculatePreparationTime())
                } else{
                    Text("Distance to restaurant: ")
                        .fontWeight(.semibold) + Text(distance)
                }
                
                Text(LocalizationCacheManager.shared.getLocalized(forKey: "Cleanness: "))
                    .fontWeight(.semibold) + Text(calculateCleannessScore())
                
                Text(restaurant.categories
                    .joined(separator: " • ")
                    .capitalized)
                .fontWeight(.semibold)
                
                Text(calculatePriceRange())
                
                    .fontWeight(.semibold)
                ReviewStarsView(stars: .constant(restaurant.stars))
                HStack(alignment: .center) {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Reviews"))
                        .frame(maxWidth: 150, maxHeight: 30)
                        .padding()
                        .background(Color.accentColor)
                        .foregroundColor(.white)
                        .cornerRadius(10)
                        .background(NavigationLink(LocalizationCacheManager.shared.getLocalized(forKey: "Reviews"), destination: ReviewsView(restaurant: restaurant)).opacity(0))
                }
            }.frame(
                minWidth: 0,
                maxWidth: .infinity,
                alignment: .topLeading
            )
        }
        .onAppear{
            imageLoader.getImage(imageName: restaurant.imageName)
        }
    }
}

struct RestaurantDetailCard_Previews: PreviewProvider {
    static var restaurants = RestaurantHostViewModel().restaurants
    static var previews: some View {
        RestaurantDetailCard(restaurant: Restaurant(name: "Toninos", stars: 5, cleannessScore: 10, categories: ["Italian", "Pasta"], dishes: [Dish(id: "1", name: "Dish 1", stars: 5, preparationTime: 5, price: 20000, imageName: "")], imageName: "", coordinates: GeoPoint(latitude: 0, longitude: 0), recommendationScore: 0, reviews: [Review(id: "1", user: "Pepito Pérez", stars: 5, text: "The experience was awesome. The dish I ordered was great and I loved how it tasted. I will be back soon")]), closest: true, distance: "50.00")
            .previewLayout(.sizeThatFits)
    }
}
