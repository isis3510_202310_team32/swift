//
//  RestaurantList.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 4/03/23.
//

import SwiftUI

struct RestaurantList: View {
    var recentlyVisitedRestaurants: [Restaurant]
    var searchedRestaurants: [Restaurant]
    var searching: Bool
    var filtering: Bool

    var body: some View {
        NavigationView {
            List {
                if searchedRestaurants.count == 0 {
                    if searching {
                        HStack {
                            Spacer()
                            NoResultsFound(message: LocalizationCacheManager.shared.getLocalized(forKey: "Ups, no results found for your search"), searching: true)
                            Spacer()
                        }
                        
                    } else if !searching && filtering {
                        HStack {
                            Spacer()
                            NoResultsFound(message: LocalizationCacheManager.shared.getLocalized(forKey: "Ups, there are no restaurants with the selected filters"), searching: false)
                            Spacer()
                        }
                    } else if !searching && !filtering {
                        HStack {
                            Spacer()
                            NoResultsFound(message: LocalizationCacheManager.shared.getLocalized(forKey: "Ups, restaurants cannot be retrieved. Please check your Internet connection"), searching: false)
                            Spacer()
                        }
                    }
                } else {
                    if recentlyVisitedRestaurants.count > 0 && !searching && !filtering {
                        Section(header: Text(LocalizationCacheManager.shared.getLocalized(forKey: "My recent visits"))) {
                            ForEach(recentlyVisitedRestaurants) { restaurant in
                                RestaurantListRow(restaurant: restaurant, closest: false, distance: "0", imageLoader: ImageCacheViewModel())
                                    .background(
                                        NavigationLink(restaurant.name,
                                                       destination: RestaurantDetail(restaurant: restaurant, closest: false, distance: "0"))
                                            .opacity(0)
                                    )
                            }
                        }
                    }
                    let listTitle = searching ? LocalizationCacheManager.shared.getLocalized(forKey: "Searched restaurants") : filtering ? LocalizationCacheManager.shared.getLocalized(forKey: "Filtered restaurants") : LocalizationCacheManager.shared.getLocalized(forKey: "Most affordable restaurants")
                    Section(header: Text(listTitle)) {
                        if searchedRestaurants.count > 0 {
                            ForEach(searchedRestaurants) { restaurant in
                                RestaurantListRow(restaurant: restaurant, closest: false, distance: "0", imageLoader: ImageCacheViewModel())
                                    .background(
                                        NavigationLink(restaurant.name,
                                                       destination: RestaurantDetail(restaurant: restaurant, closest: false, distance: "0"))
                                            .opacity(0)
                                    )
                            }
                        }
                    }
                }
            }
            .listStyle(.plain)
            .headerProminence(.increased)
        }
    }
}

struct RestaurantList_Previews: PreviewProvider {
    static var restaurants = RestaurantHostViewModel().restaurants
    static var previews: some View {
        RestaurantList(recentlyVisitedRestaurants: [restaurants[0], restaurants[1]], searchedRestaurants: restaurants, searching: false, filtering: false)
    }
}
