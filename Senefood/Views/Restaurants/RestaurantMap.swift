//
//  MapView.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 5/03/23.
//

import MapKit
import SwiftUI

struct RestaurantMap: View {
    var restaurants: [Restaurant]
    @StateObject private var mapViewModel = MapViewModel()
    @EnvironmentObject var networkManager: NetworkManager
    @ObservedObject var alertViewModel = AlertViewModel()
    @StateObject var imageLoader = ImageCacheViewModel()
    
    var body: some View {
        NavigationView {
            VStack {
                if mapViewModel.locationManager == nil || (mapViewModel.locationManager != nil && mapViewModel.locationManager?.authorizationStatus == .denied) {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Unable to fetch user location, check permission settings"))
                        .frame(maxHeight: 10)
                        .scaledToFit()
                        .minimumScaleFactor(0.7)
                } else if mapViewModel.locationManager != nil && mapViewModel.locationManager!.location == nil {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Turn on location service to share user location"))
                        .frame(maxHeight: 10)
                        .scaledToFit()
                        .minimumScaleFactor(0.7)
                } else if mapViewModel.locationManager != nil && mapViewModel.locationManager!.accuracyAuthorization == .reducedAccuracy {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "User location not precise, check accuracy authorization"))
                        .frame(maxHeight: 10)
                        .scaledToFit()
                        .minimumScaleFactor(0.7)
                }
                
                GeometryReader { geometry in
                    let layout = geometry.size.width > geometry.size.height ? AnyLayout(HStackLayout()) : AnyLayout(VStackLayout())
                    
                    layout {
                        Map(coordinateRegion: $mapViewModel.region,
                            showsUserLocation: true,
                            userTrackingMode: nil,
                            annotationItems: restaurants,
                            annotationContent: { restaurant in
                            MapAnnotation(coordinate: CLLocationCoordinate2D(latitude: restaurant.coordinates.latitude, longitude: restaurant.coordinates.longitude)) {
                                LocationMapAnnotation()
                                    .scaleEffect(mapViewModel.selectedRestaurant?.id == restaurant.id ? 1.3 : 1)
                                    .onTapGesture {
                                        mapViewModel.selectedRestaurant = restaurant
                                        mapViewModel.resetRegion()
                                        mapViewModel.calculateUserDistance()
                                        imageLoader.getImage(imageName: restaurant.imageName)
                                    }
                            }
                        })
                        .ignoresSafeArea(edges: .horizontal)
                        .onAppear {
                            mapViewModel.checkLocServicesIsEnabled()
                            if !networkManager.isConnected {
                                alertViewModel.showAlertView(alertTitle: LocalizationCacheManager.shared.getLocalized(forKey: "Ups, no internet connection"), alertMessage: LocalizationCacheManager.shared.getLocalized(forKey: "Map content will not display correctly until connection is restored"))
                            }
                        }
                        .onChange(of: geometry.size.width) { _ in
                            mapViewModel.resetRegion()
                        }
                        .onDisappear() {
                            mapViewModel.locationManager = nil
                        }
                        .alert(isPresented: $alertViewModel.showAlert,
                               content: { alertViewModel.alert() })
                        
                        if mapViewModel.selectedRestaurant != nil {
                            VStack {
                                List {
                                    RestaurantListRow(restaurant: mapViewModel.selectedRestaurant!, closest: false,distance: "0", imageLoader: imageLoader)
                                        .background(
                                            NavigationLink(mapViewModel.selectedRestaurant!.name,
                                                           destination: RestaurantDetail(restaurant: mapViewModel.selectedRestaurant!, closest: false, distance: "0"))
                                            .opacity(0)
                                        )
                                }
                                .listStyle(.plain)
                                .frame(maxHeight: 150)
                                
                                if mapViewModel.restaurantDistance != nil {
                                    Text(" \(LocalizationCacheManager.shared.getLocalized(forKey: "Distance: ")) \(mapViewModel.restaurantDistance ?? 0, specifier: "%.2f") m")
                                        .frame(maxWidth: 200)
                                        .padding(10)
                                        .font(.body)
                                        .background(Color.accentColor)
                                        .foregroundColor(.white)
                                        .cornerRadius(10)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

struct RestaurantMap_Previews: PreviewProvider {
    static var restaurants = RestaurantHostViewModel().restaurants
    static var previews: some View {
        RestaurantMap(restaurants: restaurants)
    }
}
