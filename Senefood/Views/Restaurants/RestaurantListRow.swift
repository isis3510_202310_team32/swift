//
//  RestaurantListRowView.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 3/03/23.
//

import CoreLocation
import SwiftUI

struct RestaurantListRow: View {
    var restaurant: Restaurant
    var closest: Bool
    
    var distance: String
    
    @StateObject var imageLoader: ImageCacheViewModel
    
    private func calculatePreparationTime() -> String {
        let prepTime: Double = Double(restaurant.dishes.map({$0.preparationTime}).reduce(0, +))/(Double(restaurant.dishes.count))
        return "\(prepTime) mins"
    }
    
    private func calculatePriceRange() -> String {
        let meanCost = Double(restaurant.dishes.map({$0.price}).reduce(0, +))/(Double(restaurant.dishes.count))
        var costSymbol = "?"
        switch meanCost {
        case 0...12000:
            costSymbol = "$"
        case 12000...24000:
            costSymbol =  "$$"
        default:
            costSymbol = "$$$"
        }
        return costSymbol
    }
    
    private func calculateCleannessScore() -> String {
        return "\(restaurant.cleannessScore)/10"
    }
    
    var body: some View {
        HStack {
            if imageLoader.isLoading{
                Image("logo")
                    .resizable()
                    .scaledToFill()
                    .frame(width: 125, height: 125)
            } else if let image = imageLoader.image {
                Image(uiImage: image)
                    .resizable()
                    .scaledToFill()
                    .frame(width: 125, height: 125)
            }
            
            VStack(alignment: .leading) {
                Text(restaurant.name)
                    .fontWeight(.semibold)
                
                ReviewStarsView(stars: .constant(restaurant.stars))
                
                if closest == false{
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Est. prep. time: "))
                        .fontWeight(.semibold) + Text(calculatePreparationTime())
                } else{
                    Text("Distance to restaurant: ")
                        .fontWeight(.semibold) + Text(distance)
                }
                
                Text(LocalizationCacheManager.shared.getLocalized(forKey: "Cleanness: "))
                    .fontWeight(.semibold) + Text(calculateCleannessScore())
                
                Text(restaurant.categories
                    .joined(separator: " • ")
                    .capitalized)
                .fontWeight(.semibold)
                
                Text(calculatePriceRange())
                    .fontWeight(.semibold)
            }.frame(
                minWidth: 0,
                maxWidth: .infinity,
                alignment: .topLeading
            )
        }
        .onAppear{
            imageLoader.getImage(imageName: restaurant.imageName)
        }
    }
}

struct RestaurantListRowView_Previews: PreviewProvider {
    static var restaurants = RestaurantHostViewModel().restaurants
    static var previews: some View {
        RestaurantListRow(restaurant: restaurants[0], closest: false, distance: "0", imageLoader: ImageCacheViewModel())
            .previewLayout(.sizeThatFits)
    }
}
