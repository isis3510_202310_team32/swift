//
//  ClosestRestaurantsView.swift
//  Senefood
//
//  Created by Daniel Rincon on 12/04/23.
//

import SwiftUI
import CoreLocation


struct ClosestRestaurantsView: View {
    
    @ObservedObject var closestRestaurantVM: ClosestRestaurantsViewModel
    @ObservedObject var recommendationVM: RecommendationsViewModel
    @ObservedObject var loginVM: LoginViewModel
    @ObservedObject var alertViewModel = AlertViewModel()
    
    @State var lookedRecommendations: Bool = false
    @State private var isLocationEnabled = false

    let event = EventManagerViewModel.instance
    
    let locationManager = LocationManager.shared
    
    var body: some View {
        
        NavigationView {
            if closestRestaurantVM.getRestaurantsSelected().isEmpty {
                VStack(alignment: .center){
                    
                    Image(systemName: "figure.walk")
                        .foregroundColor(Color("AccentColor"))
                        .font(.system(size: 80))
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Keep walking! We are looking for restaurants around you"))
                        .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                }
            } else{
                VStack{
                    List{
                        Section(header: Text(LocalizationCacheManager.shared.getLocalized(forKey: "Restaurants close to you, based on your preferences"))){
                            ForEach(closestRestaurantVM.getRestaurantsSelected(), id: \.0.id) { (restaurant, distance) in
                                RestaurantListRow(restaurant: restaurant, closest: true, distance: String(format: "%.0fm", distance), imageLoader: ImageCacheViewModel())
                                    .background(
                                        NavigationLink(restaurant.name,
                                                       destination: RestaurantDetail(restaurant: restaurant, closest: true, distance: String(format: "%.0fm", distance)))
                                        .opacity(0)
                                    )
                            }
                        }
                    }
                    .listStyle(.plain)
                    if !closestRestaurantVM.userPreferences{
                        Text(LocalizationCacheManager.shared.getLocalized(forKey: "You don't have any prefereces, we will show you everything"))
                    }
                }
            }
        }
        .padding(.top, 0.3)
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarItems(leading: Button(LocalizationCacheManager.shared.getLocalized(forKey: "Back"), action: {
            recommendationVM.changeView(newView: .recommendedRestaurants)
        }))
        .navigationViewStyle(StackNavigationViewStyle())
        .onAppear {
            event.sendEventFavouriteRestaurantsFeature(feature: "Location")
            print("THE CLOSEST RESTAURANTS VIEW IS ON APPEAR")
            closestRestaurantVM.triggerRecommendationSystem(username: loginVM.registeredUser!.username)
        }
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification)) { _ in
            DispatchQueue.global(qos: .background).async {
                locationManager.checkAuthorizationStatus { success in
                        DispatchQueue.main.async {
                            recommendationVM.changeView(newView: .recommendedRestaurants)
                    }
                }
            }
        }
    }
}


