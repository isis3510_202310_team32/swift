//
//  RecommendationView.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 9/03/23.
//

import SwiftUI

struct DeviceRotationViewModifier: ViewModifier {
    let action: (UIDeviceOrientation) -> Void
    
    func body(content: Content) -> some View {
        content
            .onAppear()
            .onReceive(NotificationCenter.default.publisher(for: UIDevice.orientationDidChangeNotification)) { _ in
                action(UIDevice.current.orientation)
            }
    }
}

// A View wrapper to make the modifier easier to use
extension View {
    func onRotate(perform action: @escaping (UIDeviceOrientation) -> Void) -> some View {
        modifier(DeviceRotationViewModifier(action: action))
    }
}

struct RecommendationView: View {
    @ObservedObject var recommendationVM: RecommendationsViewModel
    @ObservedObject var loginModel: LoginViewModel
    @ObservedObject var closestRestaurantVM: ClosestRestaurantsViewModel
    @EnvironmentObject var favouriteViewModel : FavouriteViewModel
    
    
    var body: some View {
        NavigationStack {
            if recommendationVM.currentView == .recommendedRestaurants {
                List {
                    Section {
                        VStack {
                            ButtonLayoutView(recommendationVM: recommendationVM)
                            Text("Last used: \(recommendationVM.lastUse ?? "Never")")
                        }.padding()
                    }
                    .listRowBackground(Color.clear)
                    .listRowSeparator(.hidden)
                    
                    Section(header: Text(LocalizationCacheManager.shared.getLocalized(forKey: "Recommended restaurants"))) {
                        HStack {
                            Text(LocalizationCacheManager.shared.getLocalized(forKey: "Our sponsor restaurants will be available here"))
                        }
                    }
                    .headerProminence(.increased)
                }
                .listStyle(.plain)
                .navigationTitle(LocalizationCacheManager.shared.getLocalized(forKey: "Recommend"))
                .navigationBarTitleDisplayMode(.inline)
                .onAppear {
                    recommendationVM.lastUseFrom(email: loginModel.user?.email ?? "n.carvajalc@uniandes.edu.co")
                }
            }
            else if recommendationVM.currentView == .preferences {
                Group {
                    RecommendationPreferencesView(recommendationVM: recommendationVM, loginVM: loginModel)
                }
                .navigationTitle(LocalizationCacheManager.shared.getLocalized(forKey: "Recommend"))
                .navigationBarTitleDisplayMode(.inline)
                .navigationBarItems(leading: Button(LocalizationCacheManager.shared.getLocalized(forKey: "Back"), action: {
                    recommendationVM.changeView(newView: .recommendedRestaurants)
                }))
            }
            else if recommendationVM.currentView == .preferencesDetails {
                Group {
                    RecommendationPreferencesDetail(recommendationVM: recommendationVM)
                }
                .navigationTitle(LocalizationCacheManager.shared.getLocalized(forKey: "Recommend"))
                .navigationBarTitleDisplayMode(.inline)
            }
            else if recommendationVM.currentView == .closestRestaurants {
                Group {
                    ClosestRestaurantsView(closestRestaurantVM: closestRestaurantVM, recommendationVM: recommendationVM, loginVM: loginModel)
                }
                .navigationTitle(LocalizationCacheManager.shared.getLocalized(forKey: "Recommend"))
            }
            else if recommendationVM.currentView == .favouriteRestaurants{
                Group{
                    FavouriteRestaurantView(loginVM: loginModel, recommendationVM: recommendationVM)
                }
                .navigationTitle(LocalizationCacheManager.shared.getLocalized(forKey: "Recommend"))
            }
        }
    }
}

struct RecommendationView_Previews: PreviewProvider {
    static var previews: some View {
        RecommendationView(recommendationVM: RecommendationsViewModel(), loginModel: LoginViewModel(), closestRestaurantVM: ClosestRestaurantsViewModel())
            .environmentObject(NetworkManager())
    }
}

struct ButtonLayoutView: View {
    @State private var orientation = UIDeviceOrientation.unknown
    @ObservedObject var recommendationVM: RecommendationsViewModel
    
    var body: some View {
        RecommendationButtons(orientation: .unknown, recommendationVM: recommendationVM)
    }
    
    struct ButtonLayoutView_Previews: PreviewProvider {
        static var previews: some View {
            ButtonLayoutView(recommendationVM: RecommendationsViewModel()).previewDevice(PreviewDevice(rawValue: "iPhone 14 Pro Max")).environmentObject(NetworkManager())
        }
    }
}
