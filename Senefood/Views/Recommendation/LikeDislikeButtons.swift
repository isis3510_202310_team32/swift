//
//  LikeDislikeButtons.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 21/03/23.
//

import SwiftUI

struct LikeDislikeButtons: View {
    var likeAction: () -> Void
    var dislikeAction: () -> Void
    var resetLikesFromDislike: () -> Void
    var resetLikesFromLike: () -> Void
    @State var isLiked: Bool
    @State var isDisliked: Bool

    var body: some View {
        HStack(spacing: 20) {
            Button(action: {
                withAnimation {
                    isLiked.toggle()
                    isDisliked = false
                }
                !isLiked ? resetLikesFromLike() : likeAction()
            }) {
                Image(systemName: "hand.thumbsup")
            }
            .scaleEffect(isLiked ? 1.2 : 1.0)
            .disabled(isDisliked)

            Button(action: {
                withAnimation {
                    isDisliked.toggle()
                    isLiked = false
                }
                !isDisliked ? resetLikesFromDislike() : dislikeAction()
            }) {
                Image(systemName: "hand.thumbsdown")
            }
            .scaleEffect(isDisliked ? 1.2 : 1.0)
            .disabled(isLiked )
        }
        .buttonStyle(.borderedProminent)
    }
}

struct LikeDislikeButtons_Previews: PreviewProvider {
    static var previews: some View {
        LikeDislikeButtons(likeAction: { print("Liked") }, dislikeAction: { print("Dislike") }, resetLikesFromDislike: { print("Reseted from dislike") }, resetLikesFromLike: { print("Reseted from like") }, isLiked: false, isDisliked: false)
    }
}
