//
//  RecommendationPreferencesView.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 21/03/23.
//

import SwiftUI

struct RecommendationPreferencesView: View {
    @EnvironmentObject var networkManager: NetworkManager
    @ObservedObject var recommendationVM: RecommendationsViewModel
    @ObservedObject var loginVM: LoginViewModel
    
    var body: some View {
        Group {
            VStack {
                Text(LocalizationCacheManager.shared.getLocalized(forKey: "We are looking for the best restaurants for you!"))
                ProgressView()
            }
            .font(.title)
            .fontWeight(.bold)
            .multilineTextAlignment(.center)
        }
        .onAppear {
            if networkManager.isConnected {
                recommendationVM.triggerRecommendationSystem(username: loginVM.registeredUser!.username, preferences: loginVM.registeredUser!.preferences)
            } else {
                recommendationVM.changeView(newView: .recommendedRestaurants)
            }
        }
    }
}

struct RecommendationPreferencesView_Previews: PreviewProvider {
    static var previews: some View {
        RecommendationPreferencesView(recommendationVM: RecommendationsViewModel(), loginVM: LoginViewModel())
    }
}
