//
//  RecommendationPreferencesDetail.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 21/03/23.
//

import SwiftUI

struct RecommendationCard: View {
    var restaurant: Restaurant
    var likes: Int
    @ObservedObject var recommendationVM: RecommendationsViewModel
    
    @StateObject private var imageLoader = ImageCacheViewModel()
    
    private func getIsLikedValue(likes: Int) -> Bool {
        if likes == 1 {
            return true
        }
        return false
    }
    
    private func getIsDislikedValue(likes: Int) -> Bool {
        if likes == -1 {
            return true
        }
        return false
    }
    
    var body: some View {
        HStack {
            if imageLoader.isLoading{
                Image("logo")
                    .resizable()
                    .scaledToFill()
                    .frame(width: 125, height: 125)
            } else if let image = imageLoader.image {
                Image(uiImage: image)
                    .resizable()
                    .scaledToFill()
                    .frame(width: 125, height: 125)
            }
            
            VStack(alignment: .leading) {
                Text(restaurant.name)
                    .fontWeight(.semibold)
                
                ReviewStarsView(stars: .constant(restaurant.stars))
                
                Text(restaurant.categories
                    .joined(separator: " • ")
                    .capitalized)
                .fontWeight(.semibold)
                LikeDislikeButtons(likeAction: { recommendationVM.likeRestaurant(restaurantId: restaurant.id) }, dislikeAction: { recommendationVM.dislikeRestaurant(restaurantId: restaurant.id) }, resetLikesFromDislike: { recommendationVM.resetLikesRestaurantFromDislike(restaurantId: restaurant.id) }, resetLikesFromLike: { recommendationVM.resetLikesRestaurantFromLike(restaurantId: restaurant.id) }, isLiked: getIsLikedValue(likes: likes), isDisliked: getIsDislikedValue(likes: likes))
                
            }.frame(
                minWidth: 0,
                maxWidth: .infinity,
                alignment: .topLeading
            )
        }
        .onAppear{
            imageLoader.getImage(imageName: restaurant.imageName)
        }
    }
}

struct RecommendationCard_Previews: PreviewProvider {
    static var previews: some View {
        EmptyView()
    }
}
