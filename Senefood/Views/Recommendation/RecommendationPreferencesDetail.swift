//
//  RecommendationPreferencesDetail.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 21/03/23.
//

import SwiftUI

struct RecommendationPreferencesDetail: View {
    let event = EventManagerViewModel.instance
    @ObservedObject var recommendationVM: RecommendationsViewModel
    var sortedRecommendations: [MappedRecommendations] {
        (recommendationVM.mappedRecommendations?.filter { $0.likes != -1 }.sorted(by: { $0.restaurant.recommendationScore > $1.restaurant.recommendationScore })) ?? []
    }

    var dislikedRecommendations: [MappedRecommendations] {
        (recommendationVM.mappedRecommendations?.filter { $0.likes == -1 }.sorted(by: { $0.restaurant.recommendationScore > $1.restaurant.recommendationScore })) ?? []
    }
    
    let eventManager = EventManagerViewModel.instance

    var body: some View {
        List {
            Section(header: Text(LocalizationCacheManager.shared.getLocalized(forKey: "Recommended for you"))) {
                ForEach(sortedRecommendations) { restaurantRecommendation in
                    RecommendationCard(restaurant: restaurantRecommendation.restaurant, likes: restaurantRecommendation.likes, recommendationVM: recommendationVM)
                }
            }
            Section(header: Text(LocalizationCacheManager.shared.getLocalized(forKey: "Disliked"))) {
                ForEach(dislikedRecommendations) { restaurantRecommendation in
                    RecommendationCard(restaurant: restaurantRecommendation.restaurant, likes: restaurantRecommendation.likes, recommendationVM: recommendationVM)
                }
            }
        }
        .listStyle(.plain)
        .onAppear{
            event.sendEventFavouriteRestaurantsFeature(feature:"Preferences")
            var likesBeginning: [MappedRecommendations] {
                (recommendationVM.mappedRecommendations?.filter { $0.likes > 0 }) ?? []
            }
            eventManager.beginningLikes = likesBeginning.count
        }
        .navigationBarItems(leading: Button(LocalizationCacheManager.shared.getLocalized(forKey: "Back"), action: {
            let recommendations: Int = sortedRecommendations.count + dislikedRecommendations.count
            
            eventManager.sendEventRecommendations(recommendations: recommendations, likeRecommendations: recommendationVM.likedRestaurants, dislikedRecommendations: dislikedRecommendations.count)
            
            recommendationVM.likedRestaurants = 0
            
            recommendationVM.changeView(newView: .recommendedRestaurants)
        }))
    }
}
