//
//  ImagePickerView.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 18/05/23.
//

import Photos
import SwiftUI
import UIKit

struct ImagePickerView: UIViewControllerRepresentable {
    @Binding var selectedImage: UIImage?
    @Environment(\.presentationMode) var isPresented
    var sourceType: UIImagePickerController.SourceType

    func makeUIViewController(context: Context) -> UIImagePickerController {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = sourceType
        imagePicker.delegate = context.coordinator // confirming the delegate

        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                if sourceType == .camera && !UIImagePickerController.isSourceTypeAvailable(.camera) {
                    showNoCameraAccessAlert()
                } else if sourceType == .camera && AVCaptureDevice.authorizationStatus(for: .video) != .authorized {
                    showNoCameraAccessAlert()
                }
            }

            // Check if the image file exists and load it
            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                let fileURL = documentsDirectory.appendingPathComponent("selectedImage.jpg")
                if FileManager.default.fileExists(atPath: fileURL.path) {
                    if let image = UIImage(contentsOfFile: fileURL.path) {
                        DispatchQueue.main.async {
                            selectedImage = image
                        }
                    }
                }
            }
        }

        return imagePicker
    }

    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {}

    // Connecting the Coordinator class with this struct
    func makeCoordinator() -> Coordinator {
        return Coordinator(picker: self)
    }

    private func showNoCameraAccessAlert() {
        let alertController = UIAlertController(
            title: "Camera Access",
            message: "Please grant camera access in Settings to capture photos.",
            preferredStyle: .alert
        )

        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            self.isPresented.wrappedValue.dismiss()
        }))

        if let topViewController = UIApplication.shared.windows.first?.rootViewController?.topMostViewController {
            topViewController.present(alertController, animated: true, completion: nil)
        }
    }
}

extension UIViewController {
    var topMostViewController: UIViewController? {
        if let presentedViewController = presentedViewController {
            return presentedViewController.topMostViewController
        }
        if let navigationController = self as? UINavigationController {
            return navigationController.visibleViewController?.topMostViewController
        }
        if let tabBarController = self as? UITabBarController {
            return tabBarController.selectedViewController?.topMostViewController
        }
        return self
    }
}

class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    var picker: ImagePickerView

    init(picker: ImagePickerView) {
        self.picker = picker
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        guard let selectedImage = info[.originalImage] as? UIImage else { return }

        // Save the image to the file system
        if let imageData = selectedImage.jpegData(compressionQuality: 1.0),
           let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        {
            let fileURL = documentsDirectory.appendingPathComponent("selectedImage.jpg")
            do {
                try imageData.write(to: fileURL)
            } catch {
                print("Failed to save image:", error)
            }
        }

        self.picker.selectedImage = selectedImage
        self.picker.isPresented.wrappedValue.dismiss()
    }
}
