//
//  ProfileSummary.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 4/03/23.
//

import SwiftUI

struct UserSummary: View {
    @ObservedObject var loginVM: LoginViewModel
    @State private var sourceType: UIImagePickerController.SourceType = .camera
    @State private var selectedImage: UIImage?
    @State private var isImagePickerDisplay = false

    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .center, spacing: 30) {
                    if selectedImage == nil {
                        ZStack {
                            loginVM.profileImage
                                .resizable()
                                .scaledToFill()
                                .frame(width: 175, height: 175, alignment: .center)
                                .clipShape(Circle())

                            Button {
                                self.isImagePickerDisplay.toggle()
                            } label: {
                                Image(systemName: "camera.fill")
                            }
                            .padding(10)
                            .background(Color("SecondaryColor"))
                            .foregroundColor(.black)
                            .clipShape(Circle())
                            .offset(x: 70, y: 65)
                        }
                        .onAppear {
                            // Load the saved image when the view appears
                            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                                let fileURL = documentsDirectory.appendingPathComponent("selectedImage.jpg")
                                if FileManager.default.fileExists(atPath: fileURL.path) {
                                    if let image = UIImage(contentsOfFile: fileURL.path) {
                                        selectedImage = image
                                    }
                                }
                            }
                        }
                    }
                    else {
                        UserPhoto(photo: Image(uiImage: selectedImage!), action: {
                            self.isImagePickerDisplay.toggle()
                        })
                        .padding(.top)
                    }

                    Text(loginVM.user?.displayName! ?? "Loading...")
                        .bold()
                        .font(.title)

                    UserPreferences(preferences: loginVM.registeredUser?.preferences ?? ["Loading..."])

                    Spacer(minLength: 15)

                    Button {
                        loginVM.signOut()
                    } label: {
                        Text(LocalizationCacheManager.shared.getLocalized(forKey: "Log Out"))
                            .frame(maxWidth: .infinity)
                    }
                    .padding(10)
                    .background(Color.accentColor)
                    .foregroundColor(.white)
                    .bold()
                    .cornerRadius(10)
                }
            }
            .padding()
            .navigationTitle(LocalizationCacheManager.shared.getLocalized(forKey: "Profile"))
            .navigationBarTitleDisplayMode(.inline)
            .sheet(isPresented: self.$isImagePickerDisplay) {
                ImagePickerView(selectedImage: self.$selectedImage, sourceType: self.sourceType)
            }
        }
    }
}

struct ProfileSummary_Previews: PreviewProvider {
    static var vm = LoginViewModel()
    static var previews: some View {
        UserSummary(loginVM: vm)
    }
}
