//
//  FirstTimeUserView.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 20/03/23.
//

import FirebaseAuth
import SwiftUI

struct FirstTimeUserView: View {
    @StateObject var vm = FirstTimeUserViewModel()
    
    var user: FirebaseAuth.User?
    @ObservedObject var loginVM: LoginViewModel
    @State var categories = retrieveRestaurantCategories()
    var atLeastOneSelected: Bool {
        categories.filter({$0.isSelected == true}).count > 0
    }

    let columns = [
        GridItem(.adaptive(minimum: 100))
    ]

    var body: some View {
        VStack {
            Spacer()
            VStack {
                Text("Welcome!")
                Text(user?.displayName ?? "Loading name...")
            }
            .font(.title)
            .fontWeight(.bold)
            .multilineTextAlignment(.center)
            Spacer()
            Text("Choose your food preferences").font(.headline).multilineTextAlignment(.leading)
            LazyVGrid(columns: columns, spacing: 20) {
                ForEach(categories.indices, id: \.self) { index in
                    CategoryChip(category: categories[index].category, isSelected: $categories[index].isSelected)
                }
            }
            .padding(.horizontal)
            Spacer()
            Button {
                vm.addPreferecesToUser(categories: categories, user: user!, loginVM: loginVM)
            } label: {
                Text("Ready")
                    .frame(maxWidth: .infinity, maxHeight: 50)
            }
            .buttonStyle(.borderedProminent)
            .padding()
            .disabled(!atLeastOneSelected)
        }
    }
}

struct FirstTimeUserView_Previews: PreviewProvider {
    static var previews: some View {
        FirstTimeUserView(user: Auth.auth().currentUser, loginVM: LoginViewModel())
    }
}
