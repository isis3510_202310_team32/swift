//
//  UserInfo.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 5/03/23.
//

import SwiftUI

struct UserPhoto: View {
    @ObservedObject var alertViewModel = AlertViewModel()
    var photo: Image
    var action: () -> Void

    var body: some View {
        ZStack {
            photo
                .resizable()
                .scaledToFill()
                .frame(width: 175, height: 175, alignment: .center)
                .clipShape(Circle())

            Button {
                action()
            } label: {
                Image(systemName: "camera.fill")
            }
            .padding(10)
            .background(Color("SecondaryColor"))
            .foregroundColor(.black)
            .clipShape(Circle())
            .offset(x: 70, y: 65)
        }
    }
}

struct UserInfo_Previews: PreviewProvider {
    static var previews: some View {
        UserPhoto(photo: Image("userPhotoF"), action: { print("Pressed camera button") })
    }
}
