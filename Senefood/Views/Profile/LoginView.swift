//
//  LoginVIew.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 9/03/23.
//

import SwiftUI

struct LoginView: View {
    @ObservedObject var loginVM: LoginViewModel
    
    var body: some View {
        ZStack {
            Color.accentColor
                .ignoresSafeArea()
            
            VStack(alignment: .center, spacing: 10) {
                Spacer()
                
                Image("logo")
                    .cornerRadius(10)
                    .padding(5)
                    .background(Color(UIColor.systemBackground))
                    .cornerRadius(10)
                    .padding()
                
                Button {
                    loginVM.signIn()
                    NotificationManager.instance.requestAuthorization()
                    NotificationManager.instance.scheduleNotification()
                    NotificationManager.instance.weekSavingsNotification()
                    
                } label: {
                    Text(LocalizationCacheManager.shared.getLocalized(forKey: "Log in with Uniandes"))
                        .font(.title2)
                        .bold()
                        .foregroundColor(Color.accentColor)
                }
                .frame(height: 50)
                .frame(maxWidth: 500)
                .background(Color(UIColor.systemBackground))
                .cornerRadius(10)
                .padding()
                
                Spacer()
            }.alert("Error", isPresented: $loginVM.hasError) {} message: {
                Text(NSLocalizedString(loginVM.errorMessage, comment: "") )
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var vm = LoginViewModel()
    
    static var previews: some View {
        LoginView(loginVM: vm)
    }
}
