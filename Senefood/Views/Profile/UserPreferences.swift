//
//  UserPreferences.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 5/03/23.
//

import SwiftUI

struct UserPreferences: View {
    var preferences: [String]

    var body: some View {
        VStack(alignment: .leading, spacing: 15) {
            HStack {
                Text(LocalizationCacheManager.shared.getLocalized(forKey: "My preferences"))
                    .font(.headline)

//                Button("Edit") {
//   /Users/nicolascarvajal/dev/personal/swift/swift/Senefood/Views/Profile/UserPreferences.swift                 //TODO: edit action
//                }
//                .padding(.horizontal, 25)
//                .padding(.vertical, 5)
//                .background(Color("SecondaryColor"))
//                .foregroundColor(.black)
//                .clipShape(Capsule())
            }

            TagCloud(tags: preferences)
        }
    }
}

struct UserPreferences_Previews: PreviewProvider {
    static var previews: some View {
        UserPreferences(preferences: ["Italian", "Asian", "Mexican", "Burguers", "Fast food", "Wok", "Colombian"])
    }
}
