//
//  BudgetConfigurationView.swift
//  Senefood
//
//  Created by Daniel Rincon on 6/03/23.
//

import SwiftUI

struct BudgetConfigurationView: View {
    var body: some View {
        NavigationView{
            VStack(alignment: .center) {
                FormBudgetView()
                
                HStack(alignment: .center, spacing: 30.0) {
                    Button(
                        action: {
                        },
                        label: {
                            Text("Reset")
                                .foregroundColor(.white)
                                .frame(width: 120.0)
                        })
                    .background(.black)
                    .cornerRadius(10)
                    .controlSize(.large)
                    
                    NavigationLink(
                        destination: BudgetView(),
                        label: {
                            Text("Done")
                                .foregroundColor(.black)
                                .frame(width: 120.0)
                                .background(.clear)
                            
                        })
                    .background(Color("SecondaryColor"))
                    .cornerRadius(10)
                    .controlSize(.large)
                }
                .padding(.bottom)
                .navigationBarTitleDisplayMode(.inline)
                .buttonStyle(.bordered)
            }
            .navigationTitle("Budget")
            .navigationBarTitleDisplayMode(.inline)
        }
        .navigationBarBackButtonHidden(true)
        .navigationViewStyle(StackNavigationViewStyle())
        
        
    }
}

struct BudgetConfigurationView_Previews: PreviewProvider {
    static var previews: some View {
        BudgetConfigurationView()
    }
}
