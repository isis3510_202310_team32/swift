//
//  DishRow.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 3/03/23.
//

import CoreLocation
import SwiftUI

struct DishRow: View {
    var dish: Dish
    
    @StateObject private var imageLoader = ImageCacheViewModel()
    
    var body: some View {
        HStack {
            
            if imageLoader.isLoading{
                Image("logo")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 120, height: 120)
            } else if let image = imageLoader.image {
                Image(uiImage: image)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 120, height: 120)
            }

            VStack(alignment: .leading) {
                Text(dish.name)
                    .fontWeight(.semibold)

                Text(LocalizationCacheManager.shared.getLocalized(forKey: "Prep. time: "))
                    .fontWeight(.semibold) + Text("\(dish.preparationTime) min")

                ReviewStarsView(stars: .constant(dish.stars))

                Text((dish.price).formatted(.currency(code: "COP")))
                    .fontWeight(.semibold)
            }.frame(
                minWidth: 0,
                maxWidth: .infinity,
                alignment: .topLeading
            )
        }
        .onAppear{
            imageLoader.getImage(imageName: dish.imageName)
        }
    }
}

struct DishRow_Previews: PreviewProvider {
    static var dish = RestaurantHostViewModel().restaurants[0].dishes[0]
    static var previews: some View {
        DishRow(dish: dish)
            .previewLayout(.sizeThatFits)
    }
}
