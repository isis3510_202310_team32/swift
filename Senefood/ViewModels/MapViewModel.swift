//
//  MapViewModel.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 5/03/23.
//

import MapKit

// Uniandes map details
enum MapDetails {
    static let startingLoc = CLLocationCoordinate2D(latitude: 4.601581544936238, longitude: -74.06614200613166)
    static let defaultSpan = MKCoordinateSpan(latitudeDelta: 0.006, longitudeDelta: 0.006)
}

final class MapViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
    @Published var selectedRestaurant: Restaurant?
    @Published var region = MKCoordinateRegion(
        center: MapDetails.startingLoc,
        span: MapDetails.defaultSpan
    )
    @Published var restaurantDistance: Double? = nil
    
    var locationManager: CLLocationManager?
    
    func checkLocServicesIsEnabled() {
        DispatchQueue.global().async {
            if CLLocationManager.locationServicesEnabled() {
                self.locationManager = CLLocationManager()
                self.locationManager?.delegate = self
            }
        }
    }
    
    private func checkLocAuth() {
        guard let locationManager = locationManager else { return }
        switch locationManager.authorizationStatus {
            
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted, .authorizedAlways, .authorizedWhenInUse, .denied:
            // Do nothing
            break
        @unknown default:
            break
        }
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocAuth()
    }
    
    func calculateUserDistance() {
        DispatchQueue.main.async {
            if self.locationManager?.location != nil && self.selectedRestaurant != nil {
                self.restaurantDistance = self.locationManager?.location!.distance(from: CLLocation(latitude: self.selectedRestaurant!.coordinates.latitude, longitude: self.selectedRestaurant!.coordinates.longitude))
            } else {
                self.restaurantDistance = nil
            }
        }
    }
    
    func resetRegion() {
        region = MKCoordinateRegion(
            center: region.center,
            span: MapDetails.defaultSpan
        )
    }
    
}
