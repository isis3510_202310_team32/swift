//
//  EventManagerViewModel.swift
//  Senefood
//
//  Created by Daniel Rincon on 23/03/23.
//

import FirebaseAnalytics
import Foundation

final class EventManagerViewModel: ObservableObject {
    static let instance = EventManagerViewModel()
    
    var beginningLikes: Int = 0
    
    
    private init() {}

    func sendEventPerCategory(restaurant: Restaurant) {
        for category in restaurant.categories {
            Analytics.logEvent("category_selected", parameters: ["category_name": category])
        }
    }

    func sendEventRestaurantPerDate(restaurant: Restaurant) {
        let restaurantName = restaurant.name.lowerSnakeCased()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let currentDate = dateFormatter.string(from: Date())
        Analytics.logEvent("restaurant_selected", parameters: ["restaurant_date": "\(restaurantName)-\(currentDate)"])
    }

    func sendEventTimeOfLaunch() {
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let formattedTime = dateFormatter.string(from: currentDate)
        print("Current time: \(formattedTime)")
        Analytics.logEvent("launch_time", parameters: ["launch_time_detail": formattedTime, "launch_time": formattedTime.split(separator: ":")[0]])
    }
    
    func sendEventRestaurantRatingReview(restaurant: Restaurant, rating: Int) {
        let restaurantName = restaurant.name.lowerSnakeCased()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM"
        let currentMonth = dateFormatter.string(from: Date())
        var qualitative = "<3"
        if rating == 3 {
            qualitative = "3"
        } else if rating > 3 {
            qualitative = ">3"
        }
        Analytics.logEvent("restaurant_rating", parameters: ["restaurant_review_stars": rating, "restaurant_reviewed": restaurantName, "restaurant_reviewed_month": currentMonth, "restaurant_review_rating": "\(restaurantName)_\(qualitative)"])
    }
    
    func sendEventRecommendations(recommendations: Int, likeRecommendations: Int, dislikedRecommendations: Int){
        if(recommendations != 0){
            let finalLikes = beginningLikes+likeRecommendations
            let currentDate = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM"
            let formattedMonth = dateFormatter.string(from: currentDate)
            let month = String(formattedMonth)
            print("MONTH: \(month)")
            // First question:
           let percentage = Int((Double(finalLikes + dislikedRecommendations) / Double(recommendations) * 100.0).rounded())
            print("THE PERCENTAGE WAS: \(percentage)")
            Analytics.logEvent("recommendations_rated_distribution", parameters: ["percentage_distribution": percentage, "month_recommendation":month])
            
            // Second question:
            let ratio = "\(finalLikes):\(dislikedRecommendations)"
            print("THE RATIO IS: \(ratio)")
            
            Analytics.logEvent("ratio_rated_frequent", parameters: ["ratio_distribution": ratio, "month_recommendation":month])
            
        } else{
            print("No recommendations were made to the user")
        }
        beginningLikes = 0
    }
    
    func sendEventBudgetSavingsWeek(budget: Int, savings: Int, week: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let currentWeek = dateFormatter.string(from: week)
        Analytics.logEvent("weekly_budget_savings", parameters: ["budget_value": budget, "budget_savings": savings, "budget_value_str": "\(budget)", "budget_savings_str": "\(savings)", "budget_week": currentWeek])
    }
    
    func sendEventFavouriteRestaurantsFeature(feature: String){
        Analytics.logEvent("favourite_restaurants_feature_event", parameters: ["favourite_restaurants_feature": feature])
    }
    
    func sendEventFavouriteRestaurantsListModified(used: String){
        Analytics.logEvent("favourite_restaurants_list_reorder", parameters: ["reorder_favourite_restaurants": used])
    }
                                                                
}
