//
//  ReviewsViewModel.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 1/04/23.
//

import FirebaseAuth
import FirebaseFirestore
import SwiftUI

final class ReviewsViewModel: ObservableObject {
    let db = Firestore.firestore()
    @Published var reviewText: String = "" {
        didSet {
            if reviewText.count > 150 {
                reviewText = String(reviewText.prefix(150))
            }
            UserDefaults.standard.set(reviewText, forKey: "reviewText")
        }
    }
    
    @Published var starRating: Int = 5 {
        didSet {
            UserDefaults.standard.set(starRating, forKey: "starRating")
        }
    }
    
    @Published var localStorageMessage = ""
    
    init() {
        self.reviewText = UserDefaults.standard.string(forKey: "reviewText") ?? ""
        self.starRating = UserDefaults.standard.integer(forKey: "starRating")
    }
    
    func defaultValues() -> Bool {
        return reviewText == "" && starRating == 0
    }
    
    func setDefaultValues() {
        reviewText = ""
        starRating = 0
        localStorageMessage = ""
    }

    func submitReview(restaurantId: String, stars: Int, text: String, user: String) {
        let restaurantRef = db.collection("Restaurants").document(restaurantId)

        restaurantRef.getDocument { document, error in
            if let error = error as NSError? {
                print("Error getting document: \(error.localizedDescription)")
            } else {
                if let document = document, document.exists {
                    do {
                        var restaurant = try document.data(as: Restaurant.self)
                        restaurant.reviews.append(Review(id: "\(UUID())", user: user, stars: stars, text: text))
                        self.recalculateGlobalScore(stars, text) { result in
                            switch result {
                            case .success(let score):
                                let rounded = round(score * 100) / 100
                                print("Review score: \(rounded)")
                                restaurant.recommendationScore += rounded

                                do {
                                    try restaurantRef.setData(from: restaurant)
                                    print("Restaurant data updated successfully")
                                } catch {
                                    print("Error updating restaurant data: \(error.localizedDescription)")
                                }

                            case .failure(let error):
                                print("Error: \(error.localizedDescription)")
                                // Handle the error
                            }
                        }
                    } catch {
                        print("Error mapping restaurant: \(error)")
                    }
                }
            }
        }
    }

    func recalculateGlobalScore(_ stars: Int, _ text: String, completion: @escaping (Result<Double, Error>) -> Void) {
        // Prepare the data to send in the POST request
        let json = ["text": text, "stars": stars] as [String: Any]
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: [])
            // Create the POST request
            let url = URL(string: "https://reviews-engine.onrender.com/calculate_review_score")!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonData

            // Send the POST request
            let task = URLSession.shared.dataTask(with: request) { data, _, error in
                if let error = error {
                    completion(.failure(error))
                } else if let data = data {
                    do {
                        let responseJson = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                        if let error = responseJson?["error"] as? String {
                            print("Error encountered in response: \(error)")
                            completion(.failure(NSError(domain: "ResponseError", code: 0, userInfo: [NSLocalizedDescriptionKey: error])))
                        } else if let score = responseJson?["review_score"] as? Double {
                            completion(.success(score))
                        } else {
                            print("Error: Could not parse review score from response.")
                            completion(.failure(NSError(domain: "ResponseError", code: 0, userInfo: [NSLocalizedDescriptionKey: "Could not parse review score from response"])))
                        }
                    } catch {
                        completion(.failure(error))
                    }
                } else {
                    print("Error: No data received in response.")
                    completion(.failure(NSError(domain: "ResponseError", code: 0, userInfo: [NSLocalizedDescriptionKey: "No data received in response"])))
                }
            }
            task.resume()
        } catch {
            print("Error serializing JSON for review sentiment analysis: \(error)")
            completion(.failure(error))
        }
    }
}
