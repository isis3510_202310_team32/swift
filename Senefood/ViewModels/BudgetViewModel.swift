//
//  BudgetViewModel.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 23/05/23.
//

import Combine
import SwiftUI

final class BudgetViewModel: ObservableObject {
    // Data
    @Published var restaurantRepository = RestaurantFirebaseServiceAdapter()
    @Published var restaurants: [Restaurant] = []
    @Published var budgetHistory: [Budget] = CoreDataManager.shared.budgetHistory()
    @Published var currentBudget: [Budget] = []
    private var cancellables: Set<AnyCancellable> = []
        
    // Budget configuration state
    @Published var showingBudgetConfigurationSheet = false
    @Published var creatingBudgetInProgress = false
    @Published var errorCreatingBudget = false
    @Published var budget: Int? {
        didSet {
            if budget != nil && budget! > 9999999 {
                budget = nil
                budgetNumberError = true
            } else { budgetNumberError = false }
        }
    }
    @Published var budgetNumberError = false
    @Published var savings = 0.0
    @Published var selectedWeekdays = [false, false, false, false, false]
    
    // Budget eligible days
    let daysFullName: [String] = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
    let daysShortName: [String] = ["Mon", "Tue", "Wed", "Thu", "Fri"]
            
    init() {
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.creatingBudgetInProgress = true
            }
            
            self.restaurantRepository.$restaurants
                .assign(to: \.restaurants, on: self)
                .store(in: &self.cancellables)
            
            DispatchQueue.main.async {
                self.currentBudget = CoreDataManager.shared.budgetsByStartDate(self.getLastSundayDate())
                self.creatingBudgetInProgress = false
            }
        }
    }
    
    func isBudgetConfigured() -> Bool {
        return currentBudget.count == 1
    }
    
    func isBudgetFormValid() -> Bool {
        return (selectedWeekdays.filter{$0}.count > 0) && budget != nil
    }
    
    func getSavingsBudget() -> Int {
        return UserDefaults.standard.integer(forKey: "budgetSavings")
    }
    
    func configureBudget() {
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.creatingBudgetInProgress = true
            }
            
            // Fetch all elegible dishes from firebase
            var allDishes: [BudgetDishUtil] = []
            for restaurant in self.restaurants {
                for dish in restaurant.dishes {
                    allDishes.append(BudgetDishUtil(restaurant: restaurant.name, name: dish.name, price: dish.price, imageURL: dish.imageName))
                }
            }
            
            // Error no dishes to choose
            if allDishes.count == 0 {
                DispatchQueue.main.async {
                    self.errorCreatingBudget = true
                    self.creatingBudgetInProgress = false
                }
                return
            }
            
            // Select dishes using the Knapsack algorithm
            var selectedDishes = self.selectDishesKnapsack(for: Int(Double(self.budget!)*(1.0-self.savings/100)), from: allDishes)
            
            // Error selected dishes do not fit budget config
            let budgetDaysCount = self.selectedWeekdays.filter{$0}.count
            if budgetDaysCount > selectedDishes.count || selectedDishes.count == allDishes.count || self.budget! < 1000 {
                DispatchQueue.main.async {
                    self.errorCreatingBudget = true
                    self.creatingBudgetInProgress = false
                }
                return
            }
            
            // Delete current budget
            for delBudget in self.currentBudget {
                CoreDataManager.shared.deleteBudget(budget: delBudget)
            }
            
            // Set new current budget
            let budgetCoreData = CoreDataManager.shared.budget(startDate: self.getLastSundayDate(), budgetMoney: Int64(self.budget!), savings: self.savings)
            
            selectedDishes.shuffle()
            var selectedDishesIndex = 0
            let dishesPerDay : Int = selectedDishes.count / budgetDaysCount
            let dishesModDay : Int = dishesPerDay + selectedDishes.count % budgetDaysCount

            for i in 0..<5 {
                if self.selectedWeekdays[i] {
                    let budgetDayCoreData = CoreDataManager.shared.budgetDay(fullName: self.daysFullName[i], shortName: self.daysShortName[i], budget: budgetCoreData)
                    var hmanyDishes = (selectedDishes.count-selectedDishesIndex == dishesModDay) ? dishesModDay : dishesPerDay
                    repeat {
                        let selectedDish = selectedDishes[selectedDishesIndex]
                        _ = CoreDataManager.shared.budgetDish(restaurant: selectedDish.restaurant, name: selectedDish.name, price: selectedDish.price, imageURL: selectedDish.imageURL, budgetDay: budgetDayCoreData)
                        selectedDishesIndex += 1
                        hmanyDishes -= 1
                    } while hmanyDishes != 0
                }
            }
            
            // Save new budget in core data
            CoreDataManager.shared.save()
            
            DispatchQueue.main.async {
                self.currentBudget = [budgetCoreData]
                self.computeSavingsBudget()
                EventManagerViewModel.instance.sendEventBudgetSavingsWeek(budget: self.budget ?? 100000, savings: self.getSavingsBudget(), week: self.getLastSundayDate())
                self.resetConfigureBudgetForm()
                self.creatingBudgetInProgress = false
            }
        }
    }
    
    func selectDishesKnapsack(for budget: Int, from dishes: [BudgetDishUtil]) -> [BudgetDishUtil] {
        let n = dishes.count
        var dp = Array(repeating: Array(repeating: 0, count: budget + 1), count: n + 1)
        
        for i in 1...n {
            for j in 1...budget {
                if Int(dishes[i - 1].price) <= j {
                    dp[i][j] = max(Int(dishes[i - 1].price) + dp[i - 1][j - Int(dishes[i - 1].price)], dp[i - 1][j])
                } else {
                    dp[i][j] = dp[i - 1][j]
                }
            }
        }
        
        var selectedDishes: [BudgetDishUtil] = []
        var i = n
        var j = budget
        
        while i > 0 && j > 0 {
            if dp[i][j] != dp[i - 1][j] {
                selectedDishes.append(dishes[i - 1])
                j -= Int(dishes[i - 1].price)
            }
            i -= 1
        }
        
        return selectedDishes
    }
    
    func computeSavingsBudget() {
        var budgetSavings = 0
        if isBudgetConfigured() {
            budgetSavings = Int(currentBudget[0].budget)
            for day in currentBudget[0].daysArray {
                for dish in day.dishesArray {
                    budgetSavings -= Int(dish.price)
                }
            }
        }
        
        UserDefaults.standard.set(budgetSavings, forKey: "budgetSavings")
    }
    
    func computeMaxDayCost() -> Int {
        var maxCost = 0
        if isBudgetConfigured() {
            for day in currentBudget[0].daysArray {
                let costDay = day.dishesArray.reduce(0) { $0 + Int($1.price) }
                maxCost = costDay > maxCost ? costDay : maxCost
            }
        }
        return maxCost
    }
    
    func resetConfigureBudgetForm() {
        self.budget = nil
        self.savings = 0.0
        self.selectedWeekdays = [false, false, false, false, false]
    }
    
    // Helpers
    func numberToMoney(number: Int) -> String {
        var numberString = String(number)
        let hundreth = numberString.suffix(3)
        
        let index = numberString.index(numberString.endIndex, offsetBy: -3)
        let thousand = numberString[..<index] // Hello
        
        numberString = "$" + String(thousand) + "." + String(hundreth)
        return numberString
    }
    
    func getLastSundayDate() -> Date {
        let dayNumberWeek = Calendar.current.component(.weekday, from: Date())
        let substractDays = -1*(dayNumberWeek - 1)
        let finalDate = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())
        return Calendar.current.date(byAdding: .day, value: substractDays, to: finalDate!)!
    }
}
