import FirebaseAuth
import FirebaseFirestore
import Kingfisher
import SwiftUI

final class LoginViewModel: ObservableObject {
    let db = Firestore.firestore()
    let imageCache = ImageCache(name: "ProfilePictureCache")
    
    @Published var hasError = false
    @Published var errorMessage = ""
    @Published var firstTimeUser = false
    
    @Published var profileImage = KFImage
        .url(URL(string: "https://firebasestorage.googleapis.com/v0/b/senefood-2b853.appspot.com/o/assets%2FuserPhoto%402x.jpeg?alt=media&token=454b9c76-b787-4b42-a740-cfb4eeafa825"))
        .placeholder { Image("userPhoto") }
        .loadDiskFileSynchronously()
        .cacheMemoryOnly()
        .fade(duration: 0.25)
        .onSuccess { _ in
            print("Succesfully retreived image")
        }
        .onFailure { _ in
            print("Error in retreived image")
        }

    var user: FirebaseAuth.User? {
        didSet {
            objectWillChange.send()
        }
    }
    
    @Published var registeredUser: Senefood.User?
    
    var provider = OAuthProvider(providerID: "microsoft.com")
    
    func listenToAuthState() {
        Auth.auth().addStateDidChangeListener { [weak self] _, user in
            guard let self = self else {
                return
            }
            self.user = user
            self.isFirstTimeUser()
        }
    }

    func signIn(
    ) {
        self.provider.getCredentialWith(nil) { credential, error in
            if error != nil {
                print("An error occured getting credentials: \(error!.localizedDescription)")
                self.hasError = true
                self.errorMessage = self.handleError(error: error)
                return
            }
            if let credential = credential {
                Auth.auth().signIn(with: credential) { _, error in
                    if error != nil {
                        print("an error occured signing in: \(error!.localizedDescription)")
                        self.hasError = true
                        self.errorMessage = self.handleError(error: error)
                        return
                    }
                }
            }
        }
    }
    
    func signOut() {
        do {
            try Auth.auth().signOut()
            self.registeredUser = nil
        } catch {
            print("Error signing out: %@", error)
            self.hasError = true
            self.errorMessage = self.handleError(error: error)
        }
    }
    
    private func handleError(error: Error?) -> String {
        var message = error!.localizedDescription
        
        if error!._code == 17020 {
            message = "Network error. Cannot login without internet connection"
        }
        return message
    }
    
    func isFirstTimeUser() {
        let key = self.user?.uid
        if key != nil {
            let docRef = self.db.collection("Users").document(key!)
            docRef.getDocument { document, _ in
                if let document = document, document.exists {
                    // Additional action if it is not the first time
                } else {
                    self.firstTimeUser = true
                }
                self.registerUser()
            }
        } else {
            print("No key was found for user")
        }
    }
    
    func registerUser() {
        if let user = self.user {
            if !self.firstTimeUser {
                let docRef = self.db.collection("Users").document(user.uid)

                docRef.getDocument { document, error in
                    if let error = error as NSError? {
                        self.errorMessage = "Error getting document: \(error.localizedDescription)"
                    } else {
                        if let document = document {
                            do {
                                self.registeredUser = try document.data(as: User.self)
                            } catch {
                                print("Error while enconding registered User \(error)")
                            }
                        }
                    }
                }
            }
        }
    }
}
