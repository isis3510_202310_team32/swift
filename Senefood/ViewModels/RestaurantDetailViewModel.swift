//
//  RestaurantDetailViewModel.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 21/03/23.
//

import Combine
import Foundation

final class RestaurantDetailViewModel: ObservableObject {
    @Published var restaurant: Restaurant
    @Published var dishRepository = DishRepository()
    @Published var dishes: [Dish] = []
    
    private var cancellables: Set<AnyCancellable> = []
    
    init() {
        dishRepository.$dishes
            .assign(to: \.dishes, on: self)
            .store(in: &cancellables)
    }
    
}
