//
//  RestaurantHostViewModel.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 20/03/23.
//

import Combine
import SwiftUI

final class RestaurantHostViewModel: ObservableObject {
    // Data
    var recentlyVisitedRepository = RecentlyVisitedFirebaseServiceAdapter()
    @Published var restaurantRepository = RestaurantFirebaseServiceAdapter()
    @Published var restaurants: [Restaurant] = []
    var recentlyVisitedRestaurants: [Restaurant] = []
    var searchedRestaurants: [Restaurant] = []
    
    @Published var searchText = "" {
        didSet {
            search()
            if searchText.count > 50 {
                searchText = String(searchText.prefix(50))
            }
        }
    }
    
    @Published var showingSheet = false
    @Published var mapView = false
    @Published var selectedRestaurant: Restaurant?
    
    // Filter state
    @Published var filteringInProgress = false
    @Published var minmaxNumberError = false
    @Published var min: Double? {
        didSet {
            if min != nil && min! > 99999 {
                min = nil
                minmaxNumberError = true
            } else { minmaxNumberError = false }
        }
    }
    @Published var max: Double? {
        didSet {
            if max != nil && max! > 99999 {
                max = nil
                minmaxNumberError = true
            } else { minmaxNumberError = false }
        }
    }
    @Published var categories: [CategoryChipAttributes] = retrieveRestaurantCategories()
    @Published var sortBy: SortingOptions = .price
    
    private var cancellables: Set<AnyCancellable> = []
    
    init() {
        restaurantRepository.$restaurants
            .assign(to: \.restaurants, on: self)
            .store(in: &cancellables)
    }
    
    func search() {
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.filteringInProgress = true
            }
            var searchedRestaurants: [Restaurant] = self.restaurants
            
            if !self.searchText.isEmpty {
                searchedRestaurants = self.restaurants.filter { self.searchText.isEmpty ? true : $0.name.lowercased().contains(self.searchText.lowercased()) }
            } else if self.filtersActive() {
                searchedRestaurants = self.filterByPrice(searchedRestaurants, self.min, self.max)
                searchedRestaurants = self.selectedCategories(self.categories).count == 0 ? searchedRestaurants : self.filterByCategories(searchedRestaurants, self.selectedCategories(self.categories))
            }
            searchedRestaurants = self.sortRestaurantsBy(searchedRestaurants, self.sortBy)
            
            self.searchedRestaurants = searchedRestaurants
            DispatchQueue.main.async {
                self.filteringInProgress = false
            }
        }
    }
    
    func filtersActive() -> Bool {
        return !(min == nil && max == nil && selectedCategories(self.categories).count == 0 && sortBy == .price)
    }
    
    func searchTextActive() -> Bool {
        return searchText.count > 0
    }
    
    func retrieveRecentlyVisitedRestaurants(for username: String) -> [Restaurant] {
        recentlyVisitedRepository.get(by: username)
        updateRecentlyVisitedRestaurant(username)
        return recentlyVisitedRestaurants
    }
    
    func addRecentlyVisitedRestaurant(_ username: String, _ restaurantId: String) {
        recentlyVisitedRepository.addRecentlyVisitedRestaurant(to: username, and: restaurantId)
        updateRecentlyVisitedRestaurant(username)
    }
    
    func updateRecentlyVisitedRestaurant(_ username: String) {
        let restaurantIds = recentlyVisitedRepository.getRestaurantIds()
        recentlyVisitedRestaurants = restaurants.filter { restaurantIds.contains($0.id!) }
    }
    
    private func filterByPrice(_ restaurantsArray: [Restaurant], _ min: Double?, _ max: Double?) -> [Restaurant] {
        if min != nil && max == nil {
            return restaurantsArray.filter { meanCost($0) > min! }
        } else if min == nil && max != nil {
            return restaurantsArray.filter { meanCost($0) < max! }
        } else if min != nil && max != nil {
            return restaurantsArray.filter { meanCost($0) > min! && meanCost($0) < max! }
        }
        return restaurantsArray
    }
    
    private func filterByCategories(_ restaurantsArray: [Restaurant], _ categories: [String]) -> [Restaurant] {
        return restaurantsArray.filter {
            var applyCat = false
            for category in $0.categories {
                applyCat = categories.contains(category.lowercased())
                if applyCat { return applyCat }
            }
            return applyCat
        }
    }
    
    private func sortRestaurantsBy(_ restaurantsArray: [Restaurant], _ sortOption: SortingOptions?) -> [Restaurant] {
        if sortOption != nil {
            switch sortOption {
            case .price, .none:
                return restaurantsArray.sorted(by: { meanCost($0) < meanCost($1) })
                
            case .cleanness:
                return restaurantsArray.sorted(by: { $0.cleannessScore > $1.cleannessScore })
                
            case .reviews:
                return restaurantsArray.sorted(by: { $0.stars > $1.stars })
                
            case .time:
                return restaurantsArray.sorted(by: { avgPrepTime($0) < avgPrepTime($1) })
            }
        }
        return restaurantsArray
    }
    
    private func avgPrepTime(_ restaurant: Restaurant) -> Double {
        return Double(restaurant.dishes.map { $0.preparationTime }.reduce(0, +))/Double(restaurant.dishes.count)
    }
    
    private func meanCost(_ restaurant: Restaurant) -> Double {
        return Double(restaurant.dishes.map { $0.price }.reduce(0, +))/Double(restaurant.dishes.count)
    }
    
    private func selectedCategories(_ categories: [CategoryChipAttributes]) -> [String] {
        return categories.filter { $0.isSelected }
            .map { $0.category.lowercased() }
    }
    
    // LAST FILTERS CACHE
    
    private struct LastFilterCache {
        static var min: Double?
        static var max: Double?
        static var selectedCategories: [CategoryChipAttributes] = retrieveRestaurantCategories()
        static var sortBy: SortingOptions = .price
    }
    
    func lastFilterCacheActive() -> Bool {
        return !(LastFilterCache.min == nil && LastFilterCache.max == nil && selectedCategories(LastFilterCache.selectedCategories).count == 0 && LastFilterCache.sortBy == .price) && !(LastFilterCache.min == self.min && LastFilterCache.max == self.max && selectedCategories(LastFilterCache.selectedCategories) == selectedCategories(self.categories) && LastFilterCache.sortBy == self.sortBy)
    }
    
    func cacheLastFilter() {
        LastFilterCache.max = self.max
        LastFilterCache.min = self.min
        LastFilterCache.selectedCategories = self.categories
        LastFilterCache.sortBy = self.sortBy
    }
    
    func eraseCacheLastFilter() {
        LastFilterCache.max = nil
        LastFilterCache.min = nil
        LastFilterCache.selectedCategories = retrieveRestaurantCategories()
        LastFilterCache.sortBy = .price
    }
    
    func loadLastFilterCache() {
        self.max = LastFilterCache.max
        self.min = LastFilterCache.min
        self.categories = LastFilterCache.selectedCategories
        self.sortBy = LastFilterCache.sortBy
    }
}


