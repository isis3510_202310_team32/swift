//
//  Senefood
//
//  Created by Daniel Rincon on 12/04/23.
//

import Combine
import CoreLocation
import Foundation
import MapKit
import SwiftUI

final class ClosestRestaurantsViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
    @Published var restaurantRepository = RestaurantFirebaseServiceAdapter()
    @Published var restaurants: [Restaurant] = []
    @Published var closestRestaurants: [(restaurant: Restaurant, distance: CLLocationDistance)] = []
    
    @Published var recommendationsPerUser: RecommendationsPerUser?
    @Published var mappedRecommendations: [MappedRecommendations]?
    @Published var userPreferences: Bool = true
    
    let recommendationService = RecommendationsService()
    
    private var cancellables: Set<AnyCancellable> = []
    
    private let locationManager = CLLocationManager()
    private var userLocation: CLLocationCoordinate2D?
    
    override init() {
        super.init()
        
        restaurantRepository.$restaurants
            .assign(to: \.restaurants, on: self)
            .store(in: &cancellables)
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            userLocation = location.coordinate
            findClosestRestaurants()
        }
        if let lastLocation = locations.dropLast().last,
           let currentLocation = locations.last,
           lastLocation.distance(from: currentLocation) >= 6
        {
            findClosestRestaurants()
        }
    }
    
    func getRestaurantsSelected() -> [(Restaurant, CLLocationDistance)] {
        if userPreferences {
            guard let mappedRecommendations = mappedRecommendations else { return [] }
            var restaurantsSelected: [(Restaurant, CLLocationDistance)] = []
            var recommendation: MappedRecommendations
            var closestRestaurant: (restaurant: Restaurant, distance: CLLocationDistance)
            for i in 0 ..< mappedRecommendations.count {
                recommendation = mappedRecommendations[i]
                if let index = closestRestaurants.firstIndex(where: { $0.restaurant.id == recommendation.restaurant.id }) {
                    closestRestaurant = closestRestaurants[index]
                    restaurantsSelected.append((recommendation.restaurant, closestRestaurant.distance))
                }
            }
            return restaurantsSelected.sorted(by: { $0.1 < $1.1 })
            
        } else {
            return closestRestaurants.sorted(by: { $0.1 < $1.1 })
        }
    }
    
    func findClosestRestaurants() {
        guard let userLocation = userLocation else { return }
        let maxDistance: CLLocationDistance = 100 // Maximum distance in meters
        let userCLLocation = CLLocation(latitude: userLocation.latitude, longitude: userLocation.longitude)
        var updatedClosestRestaurants: [(restaurant: Restaurant, distance: CLLocationDistance)] = []
        var restaurant: Restaurant
        var restaurantLocation: CLLocation
        var distance: CLLocationDistance
        for i in 0 ..< restaurants.count {
            restaurant = restaurants[i]
            restaurantLocation = CLLocation(latitude: restaurant.coordinates.latitude, longitude: restaurant.coordinates.longitude)
            distance = userCLLocation.distance(from: restaurantLocation)
            if distance <= maxDistance {
                updatedClosestRestaurants.append((restaurant: restaurant, distance: distance))
            }
        }
        closestRestaurants = updatedClosestRestaurants
    }
    
    func triggerRecommendationSystem(username: String) {
        recommendationService.getRecommendationsPerUser(username: username) { result in
            switch result {
            case .success(let recommendations):
                self.recommendationsPerUser = recommendations
                self.mapRecommendations(recommendations: self.recommendationsPerUser)
            case .failure(let error):
                self.userPreferences = false
                print("Error decoding document: \(error.localizedDescription)")
            }
        }
    }
    
    func mapRecommendations(recommendations: RecommendationsPerUser?) {
        let ids = recommendations!.recommendations.map { $0.restaurant }
        var mappedRecommendations = [MappedRecommendations]()
        
        recommendationService.getRestaurantsById(ids: ids) { result in
            switch result {
            case .success(let restaurants):
                var restaurant: Restaurant
                var mappedRestaurant: Recommendation?
                for i in 0 ..< restaurants.count {
                    restaurant = restaurants[i]
                    mappedRestaurant = recommendations?.recommendations.first(where: { $0.restaurant == restaurant.id })
                    mappedRecommendations.append(MappedRecommendations(likes: (mappedRestaurant?.likes)!, restaurant: restaurant, id: restaurant.id!))
                }
                self.mappedRecommendations = mappedRecommendations
                self.mappedRecommendations = mappedRecommendations.filter { $0.likes != -1 }
                
            case .failure(let error):
                print("Error decoding document: \(error.localizedDescription)")
            }
        }
    }
}
