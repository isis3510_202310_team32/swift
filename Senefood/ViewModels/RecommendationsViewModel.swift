//
//  RecommendationsViewModel.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 21/03/23.
//

import FirebaseFirestore
import SwiftUI

final class RecommendationsViewModel: ObservableObject {
    let db = Firestore.firestore()
    @Published var currentView: Views = .recommendedRestaurants
    @Published var recommendationsPerUser: RecommendationsPerUser?
    @Published var mappedRecommendations: [MappedRecommendations]?
    @Published var lastUse: String?
    var likedRestaurants: Int = 0
    let recommendationService = RecommendationsService()

    enum Views {
        case recommendedRestaurants, preferences, preferencesDetails, closestRestaurants, favouriteRestaurants
    }

    func changeView(newView: Views) {
        currentView = newView
    }

    func triggerRecommendationSystem(username: String, preferences _: [String]) {
        recommendationService.getRecommendationsPerUser(username: username) { result in
            switch result {
            case .success(let recommendations):
                self.recommendationsPerUser = recommendations
                self.mapRecommendations(recommendations: self.recommendationsPerUser)
            case .failure(let error):
                print("Error decoding document: \(error.localizedDescription)")
            }
        }
    }

    func mapRecommendations(recommendations: RecommendationsPerUser?) {
        let ids = recommendations!.recommendations.map { $0.restaurant }
        var mappedRecommendations = [MappedRecommendations]()

        DispatchQueue.global(qos: .userInitiated).async {
            self.recommendationService.getRestaurantsById(ids: ids) { result in
                switch result {
                case .success(let restaurants):
                    for restaurant in restaurants {
                        let mappedRestaurant = recommendations?.recommendations.first(where: { $0.restaurant == restaurant.id })
                        mappedRecommendations.append(MappedRecommendations(likes: (mappedRestaurant?.likes)!, restaurant: restaurant, id: restaurant.id!))
                    }
                    DispatchQueue.main.async {
                        self.mappedRecommendations = mappedRecommendations
                        self.changeView(newView: .preferencesDetails)
                    }
                case .failure(let error):
                    print("Error decoding document: \(error.localizedDescription)")
                }
            }
        }
    }

    func likeRestaurant(restaurantId: String?) {
        recommendationService.likeRestaurant(username: (recommendationsPerUser?.id)!, restaurantId: restaurantId!) { result in
            switch result {
            case .success():
                let index = self.mappedRecommendations!.firstIndex(where: { $0.restaurant.id == restaurantId })
                self.mappedRecommendations![index!].likes = 1
                print("Restaurant recommendation updated to 1")
            case .failure(let error):
                print("Error liking restaurant: \(error.localizedDescription)")
            }
        }
        likedRestaurants += 1
    }

    func dislikeRestaurant(restaurantId: String?) {
        recommendationService.dislikeRestaurant(username: (recommendationsPerUser?.id)!, restaurantId: restaurantId!) { result in
            switch result {
            case .success():
                let index = self.mappedRecommendations!.firstIndex(where: { $0.restaurant.id == restaurantId })
                self.mappedRecommendations![index!].likes = -1
                print("Restaurant recommendation updated to -1")
            case .failure(let error):
                print("Error disliking restaurant: \(error.localizedDescription)")
            }
        }
    }

    func resetLikesRestaurantFromDislike(restaurantId: String?) {
        recommendationService.resetLikesRestaurantFromDislike(username: (recommendationsPerUser?.id)!, restaurantId: restaurantId!) { result in
            switch result {
            case .success():
                let index = self.mappedRecommendations!.firstIndex(where: { $0.restaurant.id == restaurantId })
                self.mappedRecommendations![index!].likes = 0
                print("Restaurant recommendation updated to 0")
            case .failure(let error):
                print("Error resetLikesRestaurantFromDislike restaurant: \(error.localizedDescription)")
            }
        }
    }

    func resetLikesRestaurantFromLike(restaurantId: String?) {
        recommendationService.resetLikesRestaurantFromLike(username: (recommendationsPerUser?.id)!, restaurantId: restaurantId!) { result in
            switch result {
            case .success():
                let index = self.mappedRecommendations!.firstIndex(where: { $0.restaurant.id == restaurantId })
                self.mappedRecommendations![index!].likes = 0
                print("Restaurant recommendation updated to 0")
            case .failure(let error):
                print("Error resetLikesRestaurantFromLike restaurant: \(error.localizedDescription)")
            }
        }
        likedRestaurants -= 1
    }

    func lastUseFrom(email: String) {
        let collectionRef = db.collection("RecommendationsPerUser")

        collectionRef.document(email).getDocument { document, error in
            if let error = error {
                print("Error getting document: \(error)")
            } else if let document = document, document.exists {
                if let lastUse = document.data()?["lastUse"] as? Timestamp {
                    let currentDate = Date()
                    let lastUseDate = lastUse.dateValue()

                    let timeElapsed = currentDate.timeIntervalSince(lastUseDate)
                    let timeString = self.formatTimeElapsed(timeElapsed)
                    self.lastUse = timeString + " ago"
                } else {
                    print("lastUse attribute not found or not a valid date")
                }
            } else {
                print("Document does not exist")
            }
        }
    }

    func formatTimeElapsed(_ timeElapsed: TimeInterval) -> String {
        if timeElapsed < 60 {
            let seconds = Int(timeElapsed)
            return "\(seconds) seconds"
        } else if timeElapsed < 3600 {
            let minutes = Int(timeElapsed / 60)
            return "\(minutes) minutes"
        } else if timeElapsed < 86400 {
            let hours = Int(timeElapsed / 3600)
            return "\(hours) hours"
        } else {
            let days = Int(timeElapsed / 86400)
            return "\(days) days"
        }
    }

    func sendLastUse(email: String) {
        let collectionRef = db.collection("RecommendationsPerUser")

        let documentRef = collectionRef.document(email)

        documentRef.getDocument { document, error in
            if let error = error {
                print("Error getting document: \(error)")
            } else if let document = document, document.exists {
                let currentDate = Timestamp(date: Date())

                documentRef.updateData(["lastUse": currentDate]) { error in
                    if let error = error {
                        print("Error updating document: \(error)")
                    } else {
                        print("Last use updated successfully")
                    }
                }
            } else {
                print("Document does not exist")
            }
        }
    }
}
