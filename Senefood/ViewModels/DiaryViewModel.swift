//
//  DiaryViewModel.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 21/05/23.
//
import CoreData
import FirebaseFirestore
import Foundation

class DiaryViewModel: ObservableObject {
    @Published var isLoading: Bool = false
    @Published var restaurants: [String: String] = [:]
    private var firestoreDB: Firestore!

    init() {
        firestoreDB = Firestore.firestore()
    }

    func fetchRestaurants() {
        isLoading = true

        firestoreDB.collection("Restaurants").getDocuments { [weak self] snapshot, _ in
            defer {
                DispatchQueue.main.async {
                    self?.isLoading = false
                }
            }

            guard let documents = snapshot?.documents else {
                print("Error retrieving restaurants")
                self?.restaurants = [:]
                return
            }

            var restaurantDict: [String: String] = [:]
            var document: QueryDocumentSnapshot?
            for i in 0 ..< documents.count {
                document = documents[i]
                if let name = document!.data()["name"] as? String,
                   let imageName = document!.data()["imageName"] as? String
                {
                    restaurantDict[name] = imageName
                }
            }
            DispatchQueue.main.async {
                self?.restaurants = restaurantDict
            }
        }
    }
}
