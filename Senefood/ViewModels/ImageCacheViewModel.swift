//
//  ImageCacheViewModel.swift
//  Senefood
//
//  Created by Daniel Rincon on 25/04/23.
//

import Foundation
import SwiftUI
import UIKit

class ImageCacheViewModel: ObservableObject{
    @Published var image: UIImage? = nil
    @Published var isLoading: Bool = false
    
    let cacheManager = CacheManager.instance
    
    
    func getImage(imageName: String){
        let url = URL(string: imageName)!
        
        if let cachedImage = cacheManager.getImage(for: url){
            image = cachedImage
        } else{
            isLoading = true
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, let image = UIImage(data: data) else { return }
                self.cacheManager.cacheImage(image, for: url)
                DispatchQueue.main.async {
                    self.image = image
                    self.isLoading = false
                }
            }.resume()
        }
        
    }
}
