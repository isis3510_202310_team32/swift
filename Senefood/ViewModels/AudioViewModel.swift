//
//  SoundCacheViewModel.swift
//  Senefood
//
//  Created by Daniel Rincon on 30/05/23.
//

import Foundation
import FirebaseStorage
import AVFoundation


class AudioViewModel {
    
    static let instance = AudioViewModel()
    
    private init(){}
    
    var audioPlayer:AVAudioPlayer!
    
    func downloadFile(audioUrl: URL, completion: @escaping (URL?) -> Void) {
        
        let documentsDirectoryURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        
        let destinationURL = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
        
        print(destinationURL)
        
        if FileManager.default.fileExists(atPath: destinationURL.path){
            print("The file already exists at path")
        } else{
            URLSession.shared.downloadTask(with: audioUrl) { (tempURL, response, error) in
                guard let tempURL = tempURL, error == nil else {
                    completion(nil)
                    return
                }
                do {
                    try FileManager.default.moveItem(at: tempURL, to: destinationURL)
                    completion(destinationURL)
                } catch let error as NSError {
                    print(error.localizedDescription)
                    completion(nil)
                }
            }.resume()
        }
    }
    
    func playAudio(audioUrl: URL){
        let documentsDirectoryURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
        
        if FileManager.default.fileExists(atPath: destinationUrl.path) {
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: destinationUrl)
                audioPlayer.prepareToPlay()
                audioPlayer.play()
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            downloadFile(audioUrl: audioUrl) { [weak self] destinationURL in
                guard let destinationURL = destinationURL else {
                    return
                }
                do {
                    self?.audioPlayer = try AVAudioPlayer(contentsOf: destinationURL)
                    self?.audioPlayer.prepareToPlay()
                    self?.audioPlayer.play()
                } catch let error {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    //    func playAudio(audioUrl: URL){
    //        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    //
    //        let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
    //        print("The destinationUrl is: \(destinationUrl)")
    //        do {
    //            audioPlayer = try AVAudioPlayer(contentsOf: destinationUrl)
    //            guard let player = audioPlayer else { return }
    //
    //            player.prepareToPlay()
    //            player.play()
    //        } catch let error {
    //            print(error.localizedDescription)
    //        }
    //    }
}
