import SwiftUI

class AlertViewModel: ObservableObject {
    @Published var showAlert = false
    var alertTitle: String?
    var alertMessage: String?
    var buttonText: String = "OK"

    func showAlertView(alertTitle: String?, alertMessage: String?) {
        showAlert = true
        if alertTitle != nil {
            self.alertTitle = alertTitle
        } else {
            self.alertTitle = nil
        }

        if alertMessage != nil {
            self.alertMessage = alertMessage
        } else {
            self.alertMessage = nil
        }
    }

    func dismissAlertView() {
        showAlert = false
    }

    func alert() -> Alert {
        return Alert(title: Text(alertTitle ?? NSLocalizedString("Feature not yet implemented", comment: "")),
                     message: Text(alertMessage ?? NSLocalizedString("This feature will be available in future versions of the app 😄", comment: "")),
                     dismissButton: .default(Text(buttonText), action: {
                         self.dismissAlertView()
                     }))
    }
}
