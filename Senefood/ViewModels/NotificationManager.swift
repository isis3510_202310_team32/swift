//
//  NotificationViewModel.swift
//  Senefood
//
//  Created by Daniel Rincon on 20/03/23.
//

import Foundation
import SwiftUI
import UserNotifications

class NotificationManager{
    
    static let instance = NotificationManager() // Singleton
    
    private init(){}
    
    // Function for requestion permission from the user to send notifications
    func requestAuthorization() {
        
        // Types of notifactions the user could receive
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        
        UNUserNotificationCenter.current().requestAuthorization(options: options) {
            (success, error) in
            if error != nil {
                print("ERROR")
            } else{
                print("SUCESS")
            }
        }
    }
    
    // Function for creating a notification that is send every weekday at 12:20 p.m.
    func scheduleNotification() {
        
        let content = UNMutableNotificationContent()
        content.title = "Its almost time for lunch!"
        content.subtitle = "Come get your own recommendations"
        content.sound = .default
        
        
        let isFirstLaunch = !UserDefaults.standard.bool(forKey: "hasLaunchedBefore")
        
        if isFirstLaunch {
            UserDefaults.standard.set(true, forKey: "hasLaunchedBefore")
            
            for weekday in 2...6 { // Monday to Friday
                var dateComponents = DateComponents()
                dateComponents.hour = 12
                dateComponents.minute = 20
                dateComponents.weekday = weekday
                
                let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
                
                let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
                
                UNUserNotificationCenter.current().add(request)
            }
        }
    }
    
    // Function for creating a notification that is send every sunday at 5:00 a.m. (when budget resets)
    func weekSavingsNotification() {
        let savings = UserDefaults.standard.integer(forKey: "budgetSavings")
        let content = UNMutableNotificationContent()
        content.title = "Budget"
        content.subtitle = "This week you have saved $\(savings) COP. Remember to configure your budget for next week!"
        content.sound = .default
        
        var dateComponents = DateComponents()
        dateComponents.hour = 5
        dateComponents.minute = 0
        dateComponents.weekday = 1
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request)
    }
}
