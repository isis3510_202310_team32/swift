//
//  FirstTimeUserViewModel.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 20/03/23.
//

import FirebaseAuth
import FirebaseFirestore
import Foundation

final class FirstTimeUserViewModel: ObservableObject {
    let db = Firestore.firestore()

    func addPreferecesToUser(categories: [CategoryChipAttributes], user: FirebaseAuth.User, loginVM: LoginViewModel) {
        let selectedCategories = categories.filter { $0.isSelected }
            .map { $0.category.lowercased() }

        var initialRecommendations = [Recommendation]()

        let usersRef = db.collection("Users").document(user.uid)
        let recommendationsRef = db.collection("RecommendationsPerUser").document(user.email!)
        let recentlyVisitedRef = db.collection("RecentlyVisitedPerUser").document(user.email!)

        db.collection("Restaurants")
            .getDocuments { querySnapshot, error in
                if let error = error {
                    print("Error getting documents: \(error)")
                } else {
                    let dispatchGroup = DispatchGroup()
                    let concurrentQueue = DispatchQueue(label: "concurrentQueue", attributes: .concurrent)

                    for document in querySnapshot!.documents {
                        dispatchGroup.enter()
                        concurrentQueue.async {
                            do {
                                let restaurant = try document.data(as: Restaurant.self)
                                if self.hasSharedElement(array1: restaurant.categories, array2: selectedCategories) {
                                    initialRecommendations.append(Recommendation(likes: 0, restaurant: restaurant.id!))
                                }
                            } catch {
                                print("Error while mapping Restaurants: \(error)")
                            }
                            dispatchGroup.leave()
                        }
                    }

                    dispatchGroup.notify(queue: .main) {
                        do {
                            try recommendationsRef.setData(from: RecommendationsPerUser(recommendations: initialRecommendations))
                        } catch {
                            print("Error while saving RecommendationsPerUser \(error)")
                        }
                        do {
                            try recentlyVisitedRef.setData(from: RecentlyVisitedPerUser(recentlyVisitedRestaurants: []))
                        } catch {
                            print("Error while saving RecentlyVisitedPerUser \(error)")
                        }
                        usersRef.setData(["preferences": selectedCategories, "username": user.email!])

                        loginVM.firstTimeUser = false
                        loginVM.registeredUser = User(username: user.email!, preferences: selectedCategories)
                    }
                }
            }
    }

    func hasSharedElement<T: Hashable>(array1: [T], array2: [T]) -> Bool {
        let set1 = Set(array1)
        let set2 = Set(array2)
        return !set1.isDisjoint(with: set2)
    }
}
