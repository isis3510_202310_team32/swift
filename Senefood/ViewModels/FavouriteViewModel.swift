//
//  FavouriteViewModel.swift
//  Senefood
//
//  Created by Daniel Rincon on 25/05/23.
//

import Foundation
import CoreData
import FirebaseFirestore
import FirebaseFirestoreSwift

class FavouriteViewModel: ObservableObject{
    //@Published var restaurantsEntities: [RestaurantEntity] = []
    @Published var restaurants: [Restaurant] = []
    
    let manager = CoreDataManager.shared
    let event = EventManagerViewModel.instance

    // Get all restaurants from Core Data
    public func fetchAllRestaurants(){
        restaurants = manager.fetchAllRestaurants()
    }
    
    public func addRestaurant(username: String, restaurant: Restaurant){
        
        DispatchQueue.global(qos: .background).async {
            let db = Firestore.firestore()
            let documentRef = db.collection("FavouriteRestaurants").document(username)

            documentRef.getDocument { (document, error) in
                if let error = error {
                    print("Error retrieving document: \(error)")
                } else if let document = document, document.exists {
                    documentRef.updateData([
                        "restaurants": FieldValue.arrayUnion([restaurant.id ?? "NO ID"])
                    ])
                } else {
                    documentRef.setData([
                        "restaurants": [restaurant.id ?? "NO ID"]
                    ])
                }
            }
        }
        
        restaurants.append(restaurant)
        
        manager.addRestaurant(username: username, restaurant: restaurant, totalRestaurants: restaurants.count)
        
        fetchAllRestaurants()
    }
    
    // Remove restaurant from the Core Data
    public func removeRestaurant(restaurant: Restaurant, username: String){

        DispatchQueue.global(qos: .background).async {
            let db = Firestore.firestore()
            db.collection("FavouriteRestaurants").document(username).updateData([
                "restaurants": FieldValue.arrayRemove([restaurant.id ?? "NO ID"])
            ])
        }
        
        restaurants.removeAll(where: { $0.id == restaurant.id})
        
        manager.removeRestaurant(restaurant: restaurant)
        
        fetchAllRestaurants()
        
    }
    
    public func reorderRestaurants(from: IndexSet, destination:Int){
        manager.reorderRestaurants(from: from, destination: destination)
        
        fetchAllRestaurants()
    }
    
    func isFavourite(restaurant: Restaurant) -> Bool{
        return manager.isFavourite(restaurant: restaurant)
    }
    
    
    public func movedUsed(used: Bool){
        if used{
            event.sendEventFavouriteRestaurantsListModified(used: "Modified list")
        } else{
            event.sendEventFavouriteRestaurantsListModified(used: "Not modified list")
        }
        
    }
}
