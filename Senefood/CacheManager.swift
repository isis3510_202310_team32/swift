//
//  CacheManager.swift
//  Senefood
//
//  Created by Daniel Rincon on 25/04/23.
//


import Foundation
import SwiftUI

class CacheManager{
    
    static let instance = CacheManager()
    
    
    private init() { }
    
    lazy var imageCache: NSCache<NSString, UIImage> = {
        let cache = NSCache<NSString, UIImage>()
        cache.countLimit = 50 // the number of images the cache will hold
        
        let totalMemory = ProcessInfo.processInfo.physicalMemory
        let totalCostLimit = Int64(floor(Double(totalMemory) / 8.0))
        cache.totalCostLimit = Int(totalCostLimit)
        
        return cache
    }()
    
    func getImage(for url: URL) -> UIImage? {
        // Check if the image is already in memory cache
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString){
            return cachedImage
        }
        
        //Check if the image is avaible in disk cache
        let path = getFilePath(for: url)
        if let imageData = FileManager.default.contents(atPath: path),
           let image = UIImage(data: imageData){
            imageCache.setObject(image, forKey: url.absoluteString as NSString)
            return image
        }
        
        return nil
    }
    
    func cacheImage(_ image: UIImage, for url:URL){
        print("CACHING THE IMAGE OF THE URL: \(url)")
        // Add the image to memory cache
        imageCache.setObject(image, forKey: url.absoluteString as NSString)
        
        // Save the image to disk cache
        let path = getFilePath(for: url)
        try? image.pngData()?.write(to: URL(fileURLWithPath: path), options: .atomic)
    }
    
    
    private func getFilePath(for url:URL) -> String{
        let documentsPath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
        let fileName = url.lastPathComponent
        let filePath = "\(documentsPath)/\(fileName)"
        return filePath
    }
    
    
    
}
