//
//  RestaurantDetail.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 7/03/23.
//

import SwiftUI

struct RestaurantDetail: View {
    @EnvironmentObject var modelData: ModelData
    @Environment(\.horizontalSizeClass) var sizeClass
    @State private var searchText = ""
    @State private var showingSheet = false
    var restaurant: Restaurant

    var restaurantIndex: Int {
        modelData.restaurants.firstIndex(where: { $0.id == restaurant.id })!
    }

    var searchedDishes: [Dish] {
        restaurant.dishes.filter { searchText.isEmpty ? true : $0.name.lowercased().contains(searchText.lowercased())
        }
    }

    func filterDishes() {
        // TODO: filter restaurants
        showingSheet.toggle()
    }

    var body: some View {
        List {
            Section {
                SearchBar(searchText: $searchText)
                RestaurantDetailCard(restaurant: restaurant)
                HStack {
                    Button(action: filterDishes) {
                        Label("Filter",
                              systemImage: "line.3.horizontal.decrease.circle").foregroundColor(.accentColor)
                    }
                    .sheet(isPresented: $showingSheet) {
                        FilterSheetView()
                            .padding()
                    }
                    Spacer()

                }.padding(.horizontal)
            }
            .listRowBackground(Color.clear)
            .listRowSeparator(.hidden)
            Section(header: Text("All restaurants")) {
                ForEach(searchedDishes) { dish in
                    DishRow(dish: dish)
                        .background(
                            NavigationLink(dish.name,
                                           destination: EmptyView()) // TODO: Add dish detail
                                .opacity(0)
                        )
                }
            }
        }
        .listStyle(.plain)
        .headerProminence(.increased)
        .navigationTitle(restaurant.name)
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct RestaurantDetail_Previews: PreviewProvider {
    static let modelData = ModelData()
    static var previews: some View {
        RestaurantDetail(restaurant: modelData.restaurants[0])
    }
}
