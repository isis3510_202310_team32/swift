//
//  Dish.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 3/03/23.
//

import Foundation
import SwiftUI

struct DishPreview: Hashable, Codable, Identifiable {
    var id: Int
    var name: String
    var stars: Int
    var preparationTime: Int
    var price: Double

    private var imageName: String
    var image: Image {
        Image(imageName)
    }
}
