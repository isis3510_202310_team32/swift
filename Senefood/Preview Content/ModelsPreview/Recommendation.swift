//
//  Recommendation.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 21/03/23.
//

import Foundation

struct Recommendation: Hashable, Codable {
    var likes: Int
    var restaurant: String
}
