//
//  RecommendationsPerUser.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 21/03/23.
//

import FirebaseFirestoreSwift
import Foundation

struct RecommendationsPerUser: Hashable, Codable, Identifiable {
    @DocumentID var id: String?
    var recommendations: [Recommendation]
}
