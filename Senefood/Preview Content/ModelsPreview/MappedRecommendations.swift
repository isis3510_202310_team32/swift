//
//  MappedRecommendations.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 22/03/23.
//

import Foundation

struct MappedRecommendations: Identifiable {
    var likes: Int
    var restaurant: Restaurant
    var id: String
}
