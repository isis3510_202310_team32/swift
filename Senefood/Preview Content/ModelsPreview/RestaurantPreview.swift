//
//  Restaurant.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 3/03/23.
//

import CoreLocation
import Foundation
import SwiftUI

struct RestaurantPreview: Hashable, Codable, Identifiable {
    var id: Int
    var name: String
    var stars: Int
    var cleannessScore: Double
    var meanCost: Double
    var categories: [String]
    var dishes: [DishPreview]

    private var imageName: String
    var image: Image {
        Image(imageName)
    }

    private var coordinates: Coordinates
    var locationCoordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(
            latitude: coordinates.latitude,
            longitude: coordinates.longitude)
    }

    struct Coordinates: Hashable, Codable {
        var latitude: Double
        var longitude: Double
    }
}

