//
//  ModelData.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 3/03/23.
//

import Combine
import Foundation

final class ModelData: ObservableObject {
<<<<<<< HEAD:Senefood/Model/ModelData.swift
    @Published var restaurants: [Restaurant] = load("restaurantData.json")
=======
    @Published var restaurants: [RestaurantPreview] = load("restaurantData.json")
    @Published var user: UserPreview = load("userData.json")
>>>>>>> feature/retrieve-restaurants-from-db-and-add-gps-feature-#14:Senefood/Preview Content/ModelsPreview/ModelData.swift
}

func load<T: Decodable>(_ filename: String) -> T {
    let data: Data

    guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
    else {
        fatalError("Couldn't find \(filename) in main bundle.")
    }

    do {
        data = try Data(contentsOf: file)
    } catch {
        fatalError("Couldn't load \(filename) from main bundle:\n\(error)")
    }

    do {
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    } catch {
        fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")
    }
}
