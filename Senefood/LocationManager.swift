//
//  LocationManager.swift
//  Senefood
//
//  Created by Daniel Rincon on 26/04/23.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate, ObservableObject {
    
    static let shared = LocationManager()
    
    private let locationManager = CLLocationManager()
    
    @Published var locationConnection = false
    
    private var firstTime = true
    
    private override init() {
        super.init()
        locationManager.delegate = self
        checkAuthorizationStatus()
    }
    
    func checkLocation() -> (Bool, String) {
        let status = locationManager.authorizationStatus
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            return (true, "")
        case .notDetermined:
            let message = NSLocalizedString("location_not_determined_message", comment: "")
            return (false, message)
        case .restricted:
            let message = NSLocalizedString("location_restricted_message", comment: "")
            return (false, message)
        case .denied:
            let message = NSLocalizedString("location_denied_message", comment: "")
            return (false, message)
        @unknown default:
            let message = NSLocalizedString("location_unknown_message", comment: "")
            return (false, message)
        }
    }

    
    func checkAuthorizationStatus(completion: ((Bool) -> Void)? = nil) {
        switch locationManager.authorizationStatus {
        case .authorizedWhenInUse, .authorizedAlways:
            if firstTime{
                self.locationConnection = true
                self.firstTime = false
                completion?(true)
            } else{
                DispatchQueue.main.async {
                    self.locationConnection = true
                    completion?(true)
                }
            }
        case .denied, .restricted, .notDetermined:
            DispatchQueue.main.async {
                self.locationConnection = false
                completion?(false)
            }
        @unknown default:
            fatalError("Unexpected authorization status")
        }
        
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkAuthorizationStatus()
        //NotificationCenter.default.post(name: .locationAuthorizationChanged, object: nil)
    }
}

//extension Notification.Name {
//    static let locationAuthorizationChanged = Notification.Name("locationAuthorizationChanged")
//}
