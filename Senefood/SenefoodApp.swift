//
//  SenefoodApp.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 3/03/23.
//

import FirebaseCore
import SwiftUI

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool
    {
        FirebaseApp.configure()
        return true
    }
}

@main
struct SenefoodApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    @StateObject private var diaryVM = DiaryViewModel()

    var body: some Scene {
        let loginViewModel = LoginViewModel()
        let networkManager = NetworkManager()

        WindowGroup {
            ContentView()
                .environmentObject(loginViewModel)
                .environmentObject(networkManager)
                .environment(\.managedObjectContext, CoreDataManager.shared.persistentContainer.viewContext)
        }
    }
}

/// An extension to format strings in *snake case*.
/// Found in https://gist.github.com/adamgraham/1b5ebbc949d00b32735eb884e68e9a9a credits to @adamgraham
extension String {
    /// A collection of all the words in the string by separating out any punctuation and spaces.
    var words: [String] {
        return components(separatedBy: CharacterSet.alphanumerics.inverted).filter { !$0.isEmpty }
    }

    /// Returns a lowercased copy of the string with punctuation removed and spaces replaced
    /// by a single underscore, e.g., "the_quick_brown_fox_jumps_over_the_lazy_dog".
    ///
    /// *Lower snake case* (or, illustratively, *snake_case*) is also known as *pothole case*.
    func lowerSnakeCased() -> String {
        return self.words.map { $0.lowercased() }.joined(separator: "_")
    }

    /// Returns an uppercased copy of the string with punctuation removed and spaces replaced
    /// by a single underscore, e.g., "THE_QUICK_BROWN_FOX_JUMPS_OVER_THE_LAZY_DOG".
    ///
    /// *Upper snake case* (or, illustratively, *SNAKE_CASE*) is also known as
    /// *screaming snake case*.
    func upperSnakeCased() -> String {
        return self.words.map { $0.uppercased() }.joined(separator: "_")
    }

    /// Returns a copy of the string with punctuation removed and spaces replaced by a single
    /// underscore, e.g., "The_quick_brown_fox_jumps_over_the_lazy_dog". Upper and lower
    /// casing is maintained from the original string.
    func mixedSnakeCased() -> String {
        return self.words.joined(separator: "_")
    }
}
