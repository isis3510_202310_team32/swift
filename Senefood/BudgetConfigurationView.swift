//
//  BudgetConfigurationView.swift
//  Senefood
//
//  Created by Daniel Rincon on 6/03/23.
//

import SwiftUI

struct BudgetConfigurationView: View {
    var body: some View {
        VStack {
            NavigationView {
                FormBudgetView()
            }
            HStack(alignment: .center, spacing: 30.0) {
                Button(action: {}, label: {
                    Text("Reset")
                        .foregroundColor(.white)
                        .frame(width: 120.0)
                })
                .background(.black)
                .cornerRadius(10)
                .controlSize(.large)

                Button(action: {}, label: {
                    Text("Done")
                        .foregroundColor(.black)
                        .frame(width: 120.0)
                })
                .background(Color("SecondaryColor"))
                .cornerRadius(10)
                .controlSize(.large)
            }
            .padding(/*@START_MENU_TOKEN@*/ .all/*@END_MENU_TOKEN@*/)
            .buttonStyle(.bordered)
        }
    }
}

struct BudgetConfigurationView_Previews: PreviewProvider {
    static var previews: some View {
        BudgetConfigurationView()
    }
}
