//
//  SoundsCacheManager.swift
//  Senefood
//
//  Created by Daniel Rincon on 30/05/23.
//

import Foundation

class AudioCacheManager {
    static let shared = AudioCacheManager()
    
    private let fileManager = FileManager.default
    
    private init() {}
    
    func saveAudioData(_ data: Data, fileName: String) {
            guard let cacheDirectory = fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first else {
                return
            }
            
            let fileURL = cacheDirectory.appendingPathComponent(fileName)
            
            do {
                try data.write(to: fileURL)
                print("MP3 audio file saved in cache: \(fileURL.absoluteString)")
            } catch {
                print("Error saving MP3 audio file in cache: \(error.localizedDescription)")
            }
        }
        
        func getAudioFileURL(fileName: String) -> URL? {
            guard let cacheDirectory = fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first else {
                return nil
            }
            
            let fileURL = cacheDirectory.appendingPathComponent(fileName)
            print(fileURL)
            
            if fileManager.fileExists(atPath: fileURL.path) {
                return fileURL
            } else {
                return nil
            }
        }
}
