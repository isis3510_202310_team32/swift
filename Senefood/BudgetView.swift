//
//  BudgetView.swift
//  Senefood
//
//  Created by Daniel Rincon on 6/03/23.
//

import SwiftUI

struct BudgetView: View {
    var body: some View {
        NavigationView {
            // View stack
            VStack(alignment: .leading) {
                // Information per day stack
                DailyBudgetView()

                Text("How much are you saving?")
                    .font(.title)
                    .padding([.top, .leading, .trailing])
                Text("Based on your weekly $100.000 COP budget you will be saving $13.000! Great!")
                    .padding()

                HStack(alignment: .center) {
                    Button(action: {
                        print("Hello World")
                    }, label: {
                        Text("Configure Budget")
                            .foregroundColor(.black)
                            .frame(width: 150.0)
                    })
                    .background(Color("SecondaryColor"))
                    .cornerRadius(10)
                    .controlSize(.large)
                    .padding(EdgeInsets(top: 10, leading: 100, bottom: 6, trailing: 90))

                }.buttonStyle(.bordered)

                Spacer()
            }
            .navigationTitle("Daily Estimated Costs")
        }.padding(.horizontal)
    }
}

struct BudgetView_Previews: PreviewProvider {
    static var previews: some View {
        BudgetView()
    }
}
