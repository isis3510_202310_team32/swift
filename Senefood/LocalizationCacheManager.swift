//
//  LocalizationCacheManager.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 26/05/23.
//

import Foundation

class LocalizationCacheManager {
    static let shared = LocalizationCacheManager()
    private let cache : NSCache<NSString, NSString> = {
        let cache = NSCache<NSString, NSString>()
        cache.countLimit = 50 // the number of localized string the cache will hold
        return cache
    }()
    
    func getLocalized(forKey key: String) -> String {
        if let cachedString = cache.object(forKey: key as NSString) {
            return cachedString as String
        }
        
        let localizedString = NSLocalizedString(key, comment: "")
        cache.setObject(localizedString as NSString, forKey: key as NSString)
        return localizedString
    }
    
    func removeAllCachedStrings() {
        cache.removeAllObjects()
    }
}
