//
//  RecentlyVisitedPerUser.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 23/03/23.
//

import Foundation
import FirebaseFirestoreSwift

struct RecentlyVisitedPerUser: Hashable, Codable, Identifiable {
    @DocumentID var id: String?
    var recentlyVisitedRestaurants: [RecentlyVisitedRestaurant]
}
