//
//  User.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 4/03/23.
//

import SwiftUI
import FirebaseFirestoreSwift

struct User: Hashable, Codable, Identifiable {
    @DocumentID var id: String?
    var username: String
    var preferences: [String]
}
