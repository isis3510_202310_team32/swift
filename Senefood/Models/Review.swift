//
//  Review.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 1/04/23.
//

import SwiftUI
import FirebaseFirestoreSwift

struct Review: Identifiable, Codable {
    var id: String
    var user: String
    var stars: Int
    var text: String
}
