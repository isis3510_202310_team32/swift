//
//  Restaurant.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 3/03/23.
//

import CoreLocation
import SwiftUI
import FirebaseFirestoreSwift
import FirebaseFirestore

struct Restaurant: Identifiable, Codable {
    @DocumentID var id: String?
    var name: String
    var stars: Int
    var cleannessScore: Double
    var categories: [String]
    var dishes: [Dish]
    var imageName: String
    var coordinates: GeoPoint
    var recommendationScore: Double
    var reviews: [Review]
}
