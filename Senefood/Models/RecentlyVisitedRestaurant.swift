//
//  RecentlyVisitedRestaurant.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 23/03/23.
//

import Foundation

struct RecentlyVisitedRestaurant: Hashable, Codable {
    var date: Date
    var restaurant: String
}
