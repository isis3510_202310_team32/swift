//
//  Budget.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 24/05/23.
//

import Foundation

let weekDayNumbers = [
    "Sunday": 0,
    "Monday": 1,
    "Tuesday": 2,
    "Wednesday": 3,
    "Thursday": 4,
    "Friday": 5,
    "Saturday": 6,
]

struct BudgetDishUtil: Codable {
    var restaurant: String
    var name: String
    var price: Double
    var imageURL: String
}

extension Budget {
    public var daysArray: [BudgetDay] {
        let set = self.days as? Set<BudgetDay> ?? []
        
        return set.sorted {
            weekDayNumbers[$0.fullName ?? "Monday"]! < weekDayNumbers[$1.fullName ?? "Monday"]!
        }
    }
}

extension BudgetDay {
    public var dishesArray: [BudgetDish] {
        let set = self.dishes as? Set<BudgetDish> ?? []
        
        return set.sorted {
            $0.price < $1.price
        }
    }
}
