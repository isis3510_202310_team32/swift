//
//  Dish.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 3/03/23.
//

import SwiftUI
import FirebaseFirestoreSwift

struct Dish: Identifiable, Codable {
    var id: String
    var name: String
    var stars: Int
    var preparationTime: Int
    var price: Double
    var imageName: String
}
