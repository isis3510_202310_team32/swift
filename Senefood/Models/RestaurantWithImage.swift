//
//  RestaurantWithImage.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 20/03/23.
//

import Foundation

struct RestaurantWithImage: Codable {
    var restaurant: Restaurant
    var imageData: Data
}
