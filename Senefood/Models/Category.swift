//
//  CategoryChipAttributes.swift
//  Senefood
//
//  Created by Juan Pablo Ramirez on 22/03/23.
//

import Foundation

struct CategoryChipAttributes {
    let category: String
    var isSelected = false
}

func retrieveRestaurantCategories() -> [CategoryChipAttributes] {
    return [
        CategoryChipAttributes(category: "italian"),
        CategoryChipAttributes(category: "pasta"),
        CategoryChipAttributes(category: "chicken"),
        CategoryChipAttributes(category: "fast food"),
        CategoryChipAttributes(category: "asian"),
        CategoryChipAttributes(category: "poke"),
        CategoryChipAttributes(category: "sushi"),
        CategoryChipAttributes(category: "hamburger"),
        CategoryChipAttributes(category: "mexican")
    ]
}
