//
//  NetworkManager.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 11/04/23.
//

import Foundation
import Network

class NetworkManager: ObservableObject {
    let monitor = NWPathMonitor()
    let queue = DispatchQueue(label: "NetworkManager")
    @Published var isConnected = true

    var connectionDescription: String {
        if isConnected {
            return "Internet connection looks good!"
        } else {
            return "Ups! You are not connected"
        }
    }

    init() {
        monitor.pathUpdateHandler = { path in
            DispatchQueue.main.async {
                self.isConnected = path.status == .satisfied
            }
        }

        monitor.start(queue: queue)
    }
}
