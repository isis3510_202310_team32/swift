//
//  FormBudgetView.swift
//  Senefood
//
//  Created by Daniel Rincon on 6/03/23.
//

import SwiftUI

struct FormBudgetView: View {
    @State var budget: Double?
    @State var saving = 0.0
    @State var isEditing = false
    @State var isMonday = false
    @State var isTuesday = false
    @State var isWednesday = false
    @State var isThursday = false
    @State var isFriday = false
    @FocusState private var focusItem: Bool

    var body: some View {
        Form {
            Section(header: Text("Weekly Budget"), footer: Text("Percentage of the total budget saved")) {
                TextField("COP", value: $budget, format: .number)
                    .focused($focusItem, equals: true)
                Slider(
                    value: $saving,
                    in: 0 ... 100,
                    step: 5
                ) {
                    Text("Saving")
                } minimumValueLabel: {
                    Text("0")
                } maximumValueLabel: {
                    Text("\(Int(saving))%")
                } onEditingChanged: { editing in
                    isEditing = editing
                }.accentColor(Color("AccentColor"))
            }
            Section(header: Text("Days of the Week")) {
                Toggle(isOn: $isMonday) {
                    Text("Monday")
                }.tint(Color("SecondaryColor"))
                Toggle(isOn: $isTuesday) {
                    Text("Tuesday")
                }.tint(Color("SecondaryColor"))
                Toggle(isOn: $isWednesday) {
                    Text("Wednesday")
                }.tint(Color("SecondaryColor"))
                Toggle(isOn: $isThursday) {
                    Text("Thursday")
                }.tint(Color("SecondaryColor"))
                Toggle(isOn: $isFriday) {
                    Text("Friday")
                }.tint(Color("SecondaryColor"))
            }
        }.navigationTitle("Budget Set Up")
            .onTapGesture {
                focusItem = false
            }
    }
}

struct FormBudgetView_Previews: PreviewProvider {
    static var previews: some View {
        FormBudgetView()
    }
}
