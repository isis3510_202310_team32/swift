//
//  DishRow.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 3/03/23.
//

import CoreLocation
import SwiftUI

struct DishRow: View {
    var dish: Dish

    var body: some View {
        HStack {
            dish.image
                .resizable()
                .scaledToFill()
                .frame(width: 120, height: 120)

            VStack(alignment: .leading) {
                Text(dish.name)
                    .fontWeight(.semibold)

                Text("Prep. time: ")
                    .fontWeight(.semibold) + Text("\(dish.preparationTime) min")

                ReviewStarsView(stars: .constant(dish.stars))

                Text((dish.price).formatted(.currency(code: "COP")))
                    .fontWeight(.semibold)
            }.frame(
                minWidth: 0,
                maxWidth: .infinity,
                alignment: .topLeading
            )
        }
    }
}

struct DishRow_Previews: PreviewProvider {
    static var dishes = ModelData().restaurants[0].dishes
    static var previews: some View {
        DishRow(dish: dishes[0])
            .previewLayout(.sizeThatFits)
    }
}
