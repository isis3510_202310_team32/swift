//
//  RestaurantDetailCard.swift
//  Senefood
//
//  Created by Nicolas Carvajal on 7/03/23.
//

import CoreLocation
import SwiftUI

struct RestaurantDetailCard: View {
    var restaurant: Restaurant

    func calculateDistance(coordinates: CLLocationCoordinate2D) -> String {
        // TODO: implement distance calculation
        return "10 m"
    }

    func calculatePriceRange(meanCost: Double) -> String {
        // TODO: implement price range calculation
        return "$$$"
    }

    func calculateCleannessScore(score: Double) -> String {
        // TODO: implement cleanness score calculation
        return "8/10"
    }

    var body: some View {
        HStack {
            restaurant.image
                .resizable()
                .scaledToFill()
                .frame(width: 164, height: 164)

            VStack(alignment: .leading) {
                Text("Distance: ")
                    .fontWeight(.semibold) + Text("\(calculateDistance(coordinates: restaurant.locationCoordinate))")

                Text("Cleanness: ")
                    .fontWeight(.semibold) + Text("\(calculateCleannessScore(score: restaurant.cleannessScore))")

                Text(restaurant.categories
                    .joined(separator: " • ")
                    .capitalized)
                    .fontWeight(.semibold)

                Text(calculatePriceRange(meanCost: restaurant.meanCost))
                    .fontWeight(.semibold)
                ReviewStarsView(stars: .constant(restaurant.stars))
                HStack(alignment: .center) {
                    Button {
                        // TODO: Add review view
                    } label: {
                        Text("Reviews")
                            .frame(maxWidth: 150, maxHeight: 30)
                    }
                    .buttonStyle(.borderedProminent)
                }
            }.frame(
                minWidth: 0,
                maxWidth: .infinity,
                alignment: .topLeading
            )
        }
    }
}

struct RestaurantDetailCard_Previews: PreviewProvider {
    static var restaurants = ModelData().restaurants
    static var previews: some View {
        RestaurantDetailCard(restaurant: restaurants[0])
            .previewLayout(.sizeThatFits)
    }
}
